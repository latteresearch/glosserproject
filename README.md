# Welcome #

This repository contains the source code of the natural language processing application Glosser, which has be developed by [[http://sydney.edu.au/engineering/latte/index.shtml|the LATTE research group]] at [[http://sydney.edu.au|the University of Sydney]]. It is an open source application.

You might also be interested in our [[https://github.com/sinaptico/reviewer|Reviewer project]], that uses Glosser for automated feedback and allows for human feedback and reviewing. 

The following papers describe both systems:
1. R.A. Calvo. Affect-Aware Reflective Writing Studios. (2015). In R.A. Calvo, S.K. D’Mello, J. Gratch and A. Kappas (Eds). Chapter 33. Handbook of Affective Computing. Oxford University Press.

2. Southavilay, V, Yacef, K, Reimann P. Calvo RA “Analysis of Collaborative Writing Processes Using Revision Maps and Probabilistic Topic Models” Learning Analytics and Knowledge – LAK 2013. Leuven, Belgium, 8-12 April, 2013

3. R.A. Calvo, A. Aditomo, V. Southavilay and K. Yacef. (2012) “The use of text and process mining techniques to study the impact of feedback on students’ writing processes”. International Conference on the Learning Sciences. Sydney, pp 416-423

4. R.A Calvo, S.T O’Rourke, J. Jones, K. Yacef, P. Reimann. (2011) “Collaborative Writing Support Tools on the Cloud”. IEEE Transactions on Learning Technologies. 4 (1) pp 88-97

5. J. Villalón, P. Kearney, R.A. Calvo, P. Reimann. “Glosser: Enhanced Feedback for Student Writing Tasks”. The 8th IEEE International Conference on Advanced Learning Technologies. July 1-5, 2008. Santander, Spain.

# Acknowledgements #
This project was supported by a Google Research Award, and grants from the Australian research Council and The Office of Learning and Teaching.

# License #

All files in the repository, the documentation, and all files that will be uploaded in the future are licensed under the Apache 2.0 license.