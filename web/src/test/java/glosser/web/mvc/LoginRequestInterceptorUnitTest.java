/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.mvc;

import glosser.app.site.Site;
import glosser.app.user.User;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LoginRequestInterceptorUnitTest {
	@Test
	public void shouldLoginRemoteUser() throws Exception {
		/* These Tests don't Work as Authentication Disabled!
		MockHttpServletRequest request = new MockHttpServletRequest(null, "/glosser/home.htm");
		request.setRemoteUser("user1");
		MockHttpServletResponse response = new MockHttpServletResponse();
		User user = new User();
		user.setSite(new Site());

		LoginRequestInterceptor loginRequestInterceptor = new LoginRequestInterceptor(null, user);
		assertThat(loginRequestInterceptor.preHandle(request, response, null), is(true));
		assertThat(user.getId(), equalTo("user1"));
		*/
	}

	@Test
	public void shouldRedirectToIWrite() throws Exception {
		/* These Tests don't Work as Authentication Disabled!
		MockHttpServletRequest request = new MockHttpServletRequest(null, "/glosser/home.htm");
		MockHttpServletResponse response = new MockHttpServletResponse();
		User user = new User();

		LoginRequestInterceptor loginRequestInterceptor = new LoginRequestInterceptor(null, user);
		assertThat(loginRequestInterceptor.preHandle(request, response, null), is(false));
		assertThat(response.getRedirectedUrl(), equalTo("/"));
		*/
	}
}
