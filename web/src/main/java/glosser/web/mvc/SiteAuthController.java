/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.mvc;

import glosser.app.Glosser;
import glosser.app.site.Site;
import glosser.app.site.SiteNotFoundException;
import glosser.app.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;

public class SiteAuthController implements Controller {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private User user;

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		logger.debug("Authenticating user");
		String docId = request.getParameter("docId");
		String siteId = request.getParameter("siteId");
		Site site = null;
		// login user site
		if (siteId != null) {
			// open new site
			logger.debug("Logging in to site: " + siteId);
			site = Glosser.getSite(Long.parseLong(siteId));			
			if (site != null) {
				user.setSite(site);
				user.setDoc(null);
				user.setId(null);
			} else {
				throw new SiteNotFoundException("Site not found: " + siteId);
			}
		}

		// login user
		/* if (user.getSite().getHarvester() != null) {
			if (Site.GOOGLE_HARVESTER_TYPE.equalsIgnoreCase(user.getSite().getHarvesterType())) {
				String token = request.getParameter("token");
				if (token == null) {
					String loginUrl = AuthSubUtil.getRequestUrl(request.getRequestURL().toString()
							+ (docId == null ? "" : ("?docId=" + URLDecoder.decode(docId, "UTF-8"))), "http://docs.google.com/feeds", false, true);
					return new ModelAndView("redirect:" + loginUrl);
				} else {
					String sessionToken = AuthSubUtil.exchangeForSessionToken(token, null);
					// TODO needs to be updated/moved
				}
			}
		} */

		// redirected user
		if (docId != null) {
			String strDoHarvest="true";
			if (site.getAutoHarvest()){
				strDoHarvest="false";
			}			
			//return new ModelAndView("redirect:home.htm?docId=" + URLEncoder.encode(docId, "UTF-8") + "&doHarvest=true");
			return new ModelAndView("redirect:home.htm?docId=" + URLEncoder.encode(docId, "UTF-8") + "&doHarvest="+strDoHarvest);
		} else {
			return new ModelAndView("redirect:home.htm");
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
