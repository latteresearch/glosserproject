/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.mvc;

import glosser.app.doc.Doc;
import glosser.app.doc.DocAccessDeniedException;
import glosser.app.doc.DocNotIndexedException;
import glosser.app.site.Site;
import glosser.app.user.User;
import glosser.web.ajax.IndexerQueuer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import tml.corpus.TextDocument;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ToolRequestInterceptor implements HandlerInterceptor {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private User user;
	private IndexerQueuer indexerQueuer;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		Site site = user.getSite();

		String docId = request.getParameter("docId");
		String docRevision = request.getParameter("revision");
		String doHarvest = request.getParameter("doHarvest");

		// load doc
		if (docId != null) {
			logger.debug("opening document: " + docId + ", revision: " + docRevision);

			// check if user has access to doc
			if (site.enforceDocPermissions() && !site.getHarvester().hasUserDocPermission(user.getId(), docId)) {
				throw new DocAccessDeniedException("Document access denied: " + docId);
			}

			//harvester doc
			if (doHarvest != null) {
				if ("true".equalsIgnoreCase(doHarvest)) {
					logger.debug("queuing document for indexing now");
					getIndexerQueuer().queueDoc(docId);
					response.sendRedirect("docnotindexed.htm?docId="+docId);
				}
			}
			
			// get doc
			Doc indexedDoc = null;
			if (docRevision != null) {
				indexedDoc = site.getDocDao().getDocWhereIdAndRevision(docId, Integer.valueOf(docRevision));
			} else {
				indexedDoc = site.getDocDao().getLatestIndexedDocWhereId(docId);
			}

			// set doc
			if (indexedDoc != null) {
				user.setDoc(indexedDoc);
				TextDocument textDocument = site.getRepository().getTextDocument(indexedDoc.tmlId());
				if (textDocument == null) {
					logger.error("Document (" + indexedDoc.tmlId() +") not found in Lucene index!");
					throw new DocNotIndexedException("Document (" + indexedDoc.tmlId() +") not found in Lucene index!");
				}
				indexedDoc.setPlainText(textDocument.getContent());
			} else {
				logger.error("Document (" + docId +") not found in DB!");
				throw new DocNotIndexedException("Document (" + docId +") not found in DB!");
			}

			// set doc revisions
			List<Doc> revisions = site.getDocDao().getIndexedDocListWhereId(indexedDoc.getId());
			for (Doc revision : revisions) {
				TextDocument textDocument = site.getRepository().getTextDocument(revision.tmlId());
				if (textDocument == null) {
					logger.error("Document revision (" + revision.tmlId() +") not found in Lucene index!");
					throw new DocNotIndexedException("Document revision (" + revision.tmlId() +") not found in Lucene index!");
				}
				revision.setPlainText(textDocument.getContent());
			}
			indexedDoc.setRevisions(revisions);
		}

		// check doc is open
		if (user.getDoc() == null) {
			if(user.getSite().getReferringUrl() != null) {
				response.sendRedirect(site.getReferringUrl());
			} else {
				response.sendRedirect("doclist.htm");
			}
			return false;
		}

		return true;
	}

	@Override
	public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView)
			throws Exception {
		return;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		return;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setIndexerQueuer(IndexerQueuer indexerQueuer) {
		this.indexerQueuer = indexerQueuer;
	}

	public IndexerQueuer getIndexerQueuer() {
		return indexerQueuer;
	}
}
