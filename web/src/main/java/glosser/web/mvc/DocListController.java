/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.mvc;

import glosser.app.Glosser;
import glosser.app.doc.Doc;
import glosser.app.harvester.FileHarvester;
import glosser.app.site.Site;
import glosser.app.site.SiteNotFoundException;
import glosser.app.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocListController implements Controller {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private User user;

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
                Site site = null;

                String siteId = request.getParameter("siteId");
                if (siteId == null) {
                    //no siteId parameter, see if user object contains site
                    site = user.getSite();

                } else { //login user site with siteId siteId
                    //TODO: INSTEAD OF CREATING SITE HERE, WE COULD REDIRECT TO siteAuth.htm
                    //  AND HAVE SiteAuthController INSTANTIATE A Site FOR US AND TELL IT TO
                    //  REDIRECT BACK AFTERWARDS
                    //open new site
                    logger.debug("Logging in to site: " + siteId);
                    site = Glosser.getSite(Long.parseLong(siteId));
                    if (site != null) {
                        user.setSite(site);
                        user.setDoc(null);
                        user.setId(null);
                    } else {
                        throw new SiteNotFoundException("Site not found: " + siteId);
                    }
                }

                logger.debug("returning doclist view");

                Map<String, Object> model = new HashMap<String, Object>();
                model.put("site", site);

		if (site.getHarvester() instanceof FileHarvester) {
			if (request instanceof MultipartHttpServletRequest) {
				MultipartFile file = ((MultipartHttpServletRequest) request).getFile("pdf-file");
				if (file != null) {
					String docId = user.getId() + "-" + file.getOriginalFilename();
					int revision = 0;
					Doc latestDoc = site.getDocDao().getLatestDocWhereId(docId);
					if (latestDoc != null) {
						revision = latestDoc.getRevision() + 1;
					}

					Doc doc = new Doc();
					doc.setId(docId);
					doc.setRevision(revision);
					doc.setTitle(file.getOriginalFilename());
					doc.setOwner(user.getId());
					doc.setAuthor(user.getId());
					doc.setContent(file.getOriginalFilename());

					// save new doc revisions
					site.getDocDao().saveDoc(doc);
				}
			}

			// get the documents uploaded by the user
			List<Doc> docs = site.getHarvester().getUserDocs(user.getId());
			model.put("docs", docs);
			return new ModelAndView("page-doclist-fileupload", model);
		} else {
			// get site documents
			List<Doc> docs = site.getHarvester().getUserDocs(user.getId());
			model.put("docs", docs);
			return new ModelAndView("page-doclist", model);
		}

	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
