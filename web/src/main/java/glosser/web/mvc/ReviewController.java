/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.mvc;

import glosser.app.Glosser;
import glosser.app.doc.Doc;
import glosser.app.harvester.InMemoryHarvester;
import glosser.app.site.Site;
import glosser.app.site.SiteNotFoundException;
import glosser.app.user.User;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

public class ReviewController implements Controller {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private User user;

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		logger.debug("opening doc for review.");
		String docId = request.getParameter("docId");
		String siteId = request.getParameter("siteId");

		// login user site
		if (docId != null && siteId != null) {
			// open new site
			logger.debug("Logging in to site: " + siteId);
			Site site = Glosser.getSite(Long.parseLong(siteId));
			if (site != null) {
				request.getSession().invalidate();
				user.setSite((Site) BeanUtils.cloneBean(site));

				// override site harvester
				InMemoryHarvester inMemoryHarvester = new InMemoryHarvester();
				user.getSite().setHarvester(inMemoryHarvester);

				// add doc to dummy harvester
				Doc doc = site.getDocDao().getLatestIndexedDocWhereId(docId);
				if (doc != null) {
					ArrayList<Doc> docs = new ArrayList<Doc>();
					docs.add(doc);
					inMemoryHarvester.setDocs(docs);
					return new ModelAndView("redirect:home.htm?docId=" + java.net.URLEncoder.encode(docId, "UTF-8"));
				}
			} else {
				throw new SiteNotFoundException("Site not found: " + siteId);
			}
		}
		return new ModelAndView("redirect:home.htm");
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
