/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.mvc;

import glosser.app.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.Log4jNestedDiagnosticContextInterceptor;
import org.springframework.web.context.request.WebRequest;

public class LogRequestInterceptor extends Log4jNestedDiagnosticContextInterceptor {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private User user;

	@Override
	public void preHandle(WebRequest request) throws Exception {
		super.preHandle(request);
	}

	@Override
	public void postHandle(WebRequest request, ModelMap model) throws Exception {
		super.postHandle(request, model);

		logger.info(String.format("user=%s,site=%s,docid=%s,revision=%s", user.getId(), user.getSite() == null ? "null" : user.getSite().getId(),
				user.getDoc() == null ? "null" : user.getDoc().getId(), user.getDoc() == null ? "null" : user.getDoc().getRevision()));
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
