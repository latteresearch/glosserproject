/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.mvc;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Properties;

public class MailExceptionResolver extends SimpleMappingExceptionResolver {

	protected static final String SMTP_HOST = "smtp.gmail.com";
	protected static final String SMTP_PORT = "465";

	protected InternetAddress[] mailRecipients = null;
	protected String mailUsername;
	protected String mailPassword;

	@Override
	protected void logException(Exception ex, HttpServletRequest request) {
		super.logException(ex, request);

		// mail exception
		if(mailRecipients != null) {
			try {
				this.mailException(ex);
			} catch (MessagingException me) {
				logger.debug("Failed to mail exception", me);
			}
		}
	}

	protected void mailException(Exception ex) throws MessagingException {

		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", SMTP_HOST);
		props.put("mail.smtp.port", SMTP_PORT);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", SMTP_PORT);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");

		Session mailSession = Session.getDefaultInstance(props);

		MimeMessage message = new MimeMessage(mailSession);
		message.setSubject("Glosser Exception: " + ex.getMessage());
		message.setContent(ExceptionUtils.getFullStackTrace(ex), "text/plain");
		message.addRecipients(Message.RecipientType.TO, mailRecipients);

		Transport transport = mailSession.getTransport();
		transport.connect(mailUsername, mailPassword);
		transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}

	public void setMailRecipients(List<String> recipients) throws AddressException {
		mailRecipients = new InternetAddress[recipients.size()];
		for (int i = 0; i < recipients.size(); i++) {
			mailRecipients[i] = new InternetAddress(recipients.get(i));
		}
	}

	public String getMailUsername() {
		return mailUsername;
	}

	public void setMailUsername(String mailUsername) {
		this.mailUsername = mailUsername;
	}

	public String getMailPassword() {
		return mailPassword;
	}

	public void setMailPassword(String mailPassword) {
		this.mailPassword = mailPassword;
	}
}
