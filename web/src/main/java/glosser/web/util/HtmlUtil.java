/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.util;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.beans.StringBean;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.HeadingTag;
import org.htmlparser.tags.TableTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlUtil {

	public static String mergeHtmlWithPlainText(String plainText, String html) throws ParserException {
		// replace paragraphs
		String htmlText = plainText.replaceAll(".+", "<p>$0</p>");

		// replace tables
		Parser parser = new Parser();
		parser.setInputHTML(html);
		NodeFilter tableFilter = new TagNameFilter("TABLE");
		NodeList tableNodes = parser.extractAllNodesThatMatch(tableFilter);

		for (int i = 0; i < tableNodes.size(); i++) {
			TableTag tableTag = (TableTag) tableNodes.elementAt(i);
			StringBean sb = new StringBean();
			tableTag.accept(new AttributeRemovalVisitor(true, true));
			tableTag.accept(sb);
			String tableString = sb.getStrings().replaceAll(".+", "<p>$0</p>");

			if (tableString != null && htmlText.contains(tableString)) {
				htmlText = htmlText.substring(0, htmlText.indexOf(tableString)) + tableTag.toHtml()
						+ htmlText.substring(htmlText.indexOf(tableString) + tableString.length());
			}
		}

		return htmlText;
	}

	public static String highlightKeywords(String text, Collection<String> keywords) {
		for (String term : keywords) {
			String termRegex = "\\b" + term + "\\b";
			Pattern termPattern = Pattern.compile(termRegex, Pattern.CASE_INSENSITIVE);
			Matcher termMatcher = termPattern.matcher(text);
			text = termMatcher.replaceAll("<b>$0</b>");
		}

		return text;
	}

	public static String getHtmlSection(String sectionHeading, String html) throws ParserException {
		Parser parser = new Parser();
		parser.setInputHTML(html);
		NodeList section = new NodeList();
		String[] headings = new HeadingTag().getIds();

		for (int h = 0; h < headings.length; h++) {
			parser.reset();
			NodeFilter headingFilter = new TagNameFilter(headings[h]);
			NodeList headingsNodes = parser.extractAllNodesThatMatch(headingFilter);
			for (int i = 0; i < headingsNodes.size(); i++) {
				HeadingTag headingTag = (HeadingTag) headingsNodes.elementAt(i);
				String headingString = headingTag.toPlainTextString();
				if (headingString.equalsIgnoreCase(sectionHeading)) {
					Node node = null;
					boolean isNextHeading = false;
					while ((node = headingTag.getNextSibling()) != null && isNextHeading == false) {
						if (node instanceof HeadingTag) {
							String tagId = ((HeadingTag) node).getAttributesEx().get(0).toString();
							for (int k = 0; k <= h; k++) {
								if (tagId.equalsIgnoreCase(headings[k])) {
									isNextHeading = true;
									break;
								}
							}
						}

						if (isNextHeading == false) {
							section.add(node);
						}
					}
				}
			}
		}

		StringBean sb = new StringBean();
		section.visitAllNodesWith(sb);

		return sb.getStrings();
	}
}
