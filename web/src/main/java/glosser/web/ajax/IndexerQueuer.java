/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.ajax;

import glosser.app.HarvesterThread;
import glosser.app.doc.Doc;
import glosser.app.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexerQueuer {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private User user;

	public void queueDoc(String docId) throws Exception {
		HarvesterThread harvesterThread = user.getSite().getHarvesterThread();
		if(!harvesterThread.isDocQueued(docId)) {
			logger.debug("Queueing document for harvesting: docId=" + docId);
			harvesterThread.queueDoc(docId);
		}
	}

	public Boolean isIndexed(String docId) throws Exception {
		if(user.getSite().getHarvesterThread().isDocQueued(docId)) {
			return false;
		}
		Doc doc = user.getSite().getDocDao().getLatestDocWhereId(docId);
		if (doc == null) {
			return false;
		}
		return doc.getIndexed();
	}

	public void setUser(User user) {
		this.user = user;
	}
}
