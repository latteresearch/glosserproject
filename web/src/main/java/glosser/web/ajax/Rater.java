/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.ajax;

import glosser.app.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class Rater {
	private User user;
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private HashMap<String, Integer> ratings = new HashMap<String, Integer>();

	public boolean isRated(String page) {
		return ratings.containsKey(page);
	}

	public int loadRating(String page) {
		return ratings.get(page);
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void saveRating(String page, int rating) {
		logger.info("user=" + user.getId() + "; page=" + page + "; score=" + rating + ";");
		ratings.put(page, rating);
	}

}
