/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.web.util.HtmlUtil;
import de.danielnaber.languagetool.rules.RuleMatch;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.tags.Span;
import org.htmlparser.util.NodeList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pt.tumba.spell.DefaultWordFinder;
import pt.tumba.spell.SpellChecker;
import tml.corpus.TextDocument;
import tml.vectorspace.operations.GrammarChecker;
import tml.vectorspace.operations.results.GrammarCheckerResult;

import java.util.*;

/**
 * Implements a spell and grammar checking Tool
 * 
 *  The LanguageTool is using http://jaspell.sourceforge.net/ 
 * @author rafa
 *
 */
@Tool(id = "language", name = "Language")
public class LanguageTool extends AbstractTool {

	@RequestMapping("/language.htm")
	public ModelAndView handleRequest() throws Exception {
		logger.debug("returning language view");
		Doc doc = user.getDoc();
		String text = doc.getPlainText();
		String html = doc.getContent();
		String htmlGloss = HtmlUtil.mergeHtmlWithPlainText(text, html);	
		TextDocument textDocument = user.getSite().getRepository().getTextDocument(doc.tmlId());
		textDocument.load(user.getSite().getRepository());
		
		// check grammar
		GrammarChecker grammarChecker = new GrammarChecker();
		grammarChecker.setCorpus(textDocument.getSentenceCorpus());
		grammarChecker.start();
		List<GrammarCheckerResult> results = grammarChecker.getResults();
		for(GrammarCheckerResult result : results) {
			String paragraphHtml = new String();
			String paragraphText = user.getSite().getRepository().getTextDocument(result.getTextDocumentId()).getContent();
			if(htmlGloss.contains(paragraphText)) {
				int first = 0;
				for(RuleMatch rule : result.getGrammarErrors()){
					try {				
						Span sentenceSpan = new Span();
						sentenceSpan.setAttribute("class", "grammarError");
						sentenceSpan.setAttribute("title", "Rule: " + rule.getMessage());
						NodeList children = new NodeList();
						children.add(new TextNode(paragraphText.substring(rule.getFromPos(), rule.getToPos())));
						sentenceSpan.setChildren(children);
						Span endSpan = new Span();
						endSpan.setTagName("/" + endSpan.getTagName());
						sentenceSpan.setEndTag(endSpan);
						paragraphHtml += paragraphText.substring(first, rule.getFromPos()) + sentenceSpan.toHtml();
						first = rule.getToPos();
					} catch (Exception e) {
						logger.error("Error", e);
					}
				}
				paragraphHtml += paragraphText.substring(first, paragraphText.length());
				htmlGloss = htmlGloss.substring(0, htmlGloss.indexOf(paragraphText)) + paragraphHtml
					+ htmlGloss.substring(htmlGloss.indexOf(paragraphText) + (paragraphText).length());
			}
			
		}

		// check spelling
		SpellChecker spellchecker = new SpellChecker();
		spellchecker.initialize("dict/english.txt");	
		Set<String> words = new HashSet<String>(Arrays.asList(DefaultWordFinder.splitWords(text)));
		for(String word : words) {
			// check if word is a number.
			if(word.matches("\\W?\\d+([:,.]\\d+)?\\W?"))
				continue;
			
			if (!(spellchecker.spellCheckWord(word).startsWith("<plain>"))) {
				List<String> suggestions = spellchecker.findMostSimilarList(word);
				Span wordSpan = new Span();
				wordSpan.setAttribute("class", "spellingError");
				wordSpan.setAttribute("title", "Suggestions: " + suggestions);
				NodeList children = new NodeList();
				children.add(new TextNode(word));
				wordSpan.setChildren(children);
				Span endSpan = new Span();
				endSpan.setTagName("/" + endSpan.getTagName());
				wordSpan.setEndTag(endSpan);
				// don't replace text that matches a tag
				htmlGloss = htmlGloss.replaceAll("([^<\\w])"+word+"([^>\\w])", "$1"+wordSpan.toHtml()+"$2");
			}
		}
		
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("htmlGloss", htmlGloss);
		return new ModelAndView("tool-language", model);
	}
}
