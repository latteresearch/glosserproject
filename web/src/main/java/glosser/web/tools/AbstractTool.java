/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
/**
 * Abstract Class that all tools need to implement.
 * 
 * Basic example to follow:
 * @see HomeTool.java
 * 
 * A tool defines a view for analyzing a specific feature (e.g. flow or spelling) of a document.
 * 
 *  Glosser provides a simple and extensible way for new tools to be installed without any modification 
 *  to the original source code. 
 *  The procedure for creating a tool requires the implementation of a Tool class, 
 *  which performs an operation on the document content and returns a view. 
 *  This class is also required to be annotated with metadata, which is read by Glosser at runtime to make the 
 *  tool available for use.
 *  
 *  The questions and everything else in Glosser are configured using the Reviewer admin. 
 *  This is a big problem as it is impossible to create new sites in Glosser without using Reviewer. 
 *  What we really need to do is create an admin section in Glosser.
 *  
 *  Tools are often based on individual research projects. 
 *  
 *  Required libraries should be included in maven repo. (what about 3rd party dependencies?)
 *  The 'status' of each should be explicit in each implementation:
 *  <ul>
 * 	<li>Production ready. It is ready to be used in class. Thoroughly tested functionally and for scalability (issues should be noted)
 * 	<li>Untested. Should be tested by Glosser code manager.
 * 	<li>Research
 * </ul>
 *  
 * @author rafa
 *
 *<p>
 * Tool Contributors:
 * <ul>
 * 	<li>Stephen O'Rourke: FlowMap, Flow, Language, Participation, 
 * 	<li>Ming Liu: Question
 * 	<li>Jorge Villalon: Concept Map 
 * </ul>
 */
@Controller
public abstract class AbstractTool {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	protected User user;

}
