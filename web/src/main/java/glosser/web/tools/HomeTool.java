/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.app.doc.DocStatistics;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Most basic tool. Shows the original document and some basic stats.
 * 
 * Eventually this could show compliance with word length for the particular assignment. 
 * But this requires specific values for Activity to be set and the there is not functionality for this.
 * 
 * <b>Status: production</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "home", name = "Home")
public class HomeTool extends AbstractTool {
	@RequestMapping("/home.htm")
	public ModelAndView handleRequest() throws Exception {
        logger.debug("returning home view");
        Doc doc = user.getDoc();

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("docStats", new DocStatistics(doc));
		model.put("htmlGloss", doc.getContent());

		return new ModelAndView("tool-home", model);
	}

}
