/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.app.doc.DocStatistics;
import glosser.app.site.Site;
import glosser.web.util.HtmlUtil;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.Span;
import org.htmlparser.util.NodeList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tml.corpus.TextDocument;
import tml.vectorspace.NoDocumentsInCorpusException;
import tml.vectorspace.operations.PassageDistances;
import tml.vectorspace.operations.Readability;
import tml.vectorspace.operations.results.PassageDistancesResult;
import tml.vectorspace.operations.results.ReadabilityResult;

import java.util.HashMap;
import java.util.List;

/**
 * This tool implements a warning system for visualizing breaks in an argument's flow
 * 
 * The FlowTool gives warnings about breaks in both argument flow and readability flow between consecutive paragraphs. 
 * Although we have not been using the readability slider as we were unsure of it's usefulness.
 * <b>Status: production</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "flow", name = "Flow")
public class FlowTool extends AbstractTool {
	private double readingEaseThreshold = 14.68;
	private double distanceThreshold = 0.35;

	@RequestMapping("/flow.htm")
	public ModelAndView handleRequest() throws NoDocumentsInCorpusException, Exception {
        logger.debug("returning flow view");
        Site site = user.getSite();
        Doc doc = user.getDoc();
		DocStatistics docStats = new DocStatistics(doc);

		// get document
		TextDocument textDocument = site.getRepository().getTextDocument(doc.tmlId());
		textDocument.load(site.getRepository());

		// segment distance analysis
		PassageDistances passageDistances = new PassageDistances();
		passageDistances.setCorpus(textDocument.getParagraphCorpus());
		passageDistances.start();
		List<PassageDistancesResult> distanceResults = passageDistances.getResults();

		// add dummy result for last paragraph
		PassageDistancesResult lastParagraphResult = new PassageDistancesResult();
		lastParagraphResult.setDistance(0);
		lastParagraphResult.setDocumentAId(distanceResults.get(distanceResults.size() - 1).getDocumentBId());
		distanceResults.add(lastParagraphResult);

		// readability analysis
		Readability readability = new Readability();
		readability.setCorpus(textDocument.getParagraphCorpus());
		readability.start();
		List<ReadabilityResult> readabilityResults = readability.getResults();

		// create html text
		String text = doc.getPlainText();
		String html = doc.getContent();
		String htmlGloss = HtmlUtil.mergeHtmlWithPlainText(text, html);

		// replace paragraphs
		for (int i = 0; i < distanceResults.size() && i < readabilityResults.size(); i++) {
			String paragraphText = readabilityResults.get(i).getTextPassageContent();
			if (htmlGloss.contains("<p>" + paragraphText + "</p>")) {
				// paragraph stats
				Doc paragraph = docStats.getOriginalSegment(paragraphText);
				DocStatistics paragraphStats = new DocStatistics(paragraph);

				// create paragraph span
				Span paragraphSpan = new Span();
				paragraphSpan.setAttribute("name", "paragraph");
				paragraphSpan.setAttribute("sentences", String.valueOf(paragraphStats.getSentenceCount()));
				paragraphSpan.setAttribute("title", String.format("Revision %s, %s, %s", paragraph.getRevision(), paragraph.getAuthor(), paragraph.getDate()));
				NodeList children = new NodeList();
				children.add(new TextNode(paragraphText));
				paragraphSpan.setChildren(children);
				Span endSpan = new Span();
				endSpan.setTagName("/" + endSpan.getTagName());
				paragraphSpan.setEndTag(endSpan);

				// check if next paragraph exists
				if (i < distanceResults.size() - 1 && i < readabilityResults.size() - 1
						&& htmlGloss.contains("<p>" + readabilityResults.get(i + 1).getTextPassageContent() + "</p>")) {
					paragraphSpan.setAttribute("distance", String.valueOf(1 - Math.min(1, Math.max(0, distanceResults.get(i).getDistance()))));
					paragraphSpan.setAttribute("readability", String.valueOf(readabilityResults.get(i).getDiffReadingEase()));
				} else {
					paragraphSpan.setAttribute("distance", String.valueOf(0));
					paragraphSpan.setAttribute("readability", String.valueOf(0));
				}

				// create distance icon
				Span distanceIcon = new Span();
				distanceIcon.setAttribute("name", "icon-distance");
				distanceIcon.setAttribute("class", "icon-none");
				distanceIcon.setEndTag(endSpan);

				// create readability icon
				Span readabilityIcon = new Span();
				readabilityIcon.setAttribute("name", "icon-readability");
				readabilityIcon.setAttribute("class", "icon-none");
				readabilityIcon.setEndTag(endSpan);

				// create icons div
				Div iconsDiv = new Div();
				NodeList icons = new NodeList();
				icons.add(distanceIcon);
				// icons.add(readabilityIcon);
				iconsDiv.setChildren(icons);
				Div endDiv = new Div();
				endDiv.setTagName("/" + endDiv.getTagName());
				iconsDiv.setEndTag(endDiv);

				// update html gloss
				htmlGloss = htmlGloss.substring(0, htmlGloss.indexOf("<p>" + paragraphText + "</p>")) + paragraphSpan.toHtml() + iconsDiv.toHtml()
						+ htmlGloss.substring(htmlGloss.indexOf("<p>" + paragraphText + "</p>") + ("<p>" + paragraphText + "</p>").length());
			}
		}

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("maxDistance", 1.0);
		model.put("maxReadability", 100.0);
		model.put("distanceThreshold", 1.0 - distanceThreshold);
		model.put("readabilityThreshold", readingEaseThreshold);
		model.put("htmlGloss", htmlGloss);

		return new ModelAndView("tool-flow", model);
	}

	public double getDistanceThreshold() {
		return distanceThreshold;
	}

	public void setDistanceThreshold(double distanceThreshold) {
		this.distanceThreshold = distanceThreshold;
	}

	public double getReadingEaseThreshold() {
		return readingEaseThreshold;
	}

	public void setReadingEaseThreshold(double readingEaseThreshold) {
		this.readingEaseThreshold = readingEaseThreshold;
	}
}
