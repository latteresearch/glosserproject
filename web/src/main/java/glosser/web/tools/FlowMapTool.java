/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.app.site.Site;
import datalib.Constants.DimensionalityReduction;
import datalib.Constants.TermWeighting;
import datalib.doc.LatentSemanticAnalysis;
import docmaps.maps.paragraph.ParagraphMapBuilder;
import docmaps.maps.paragraph.ParagraphMapVis;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import prefuse.Display;
import tml.corpus.ParagraphCorpus;
import tml.corpus.SentenceCorpus;
import tml.corpus.TextDocument;
import tml.vectorspace.factorisation.MultiDimensionalScalingNR;
import vislib.map.MapVis;
import weka.core.Instances;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.*;

/**
 * Shows a visualization of topic flows.
 * 
 * Based on Stephen O'Rourke MPhil thesis. Described in 
 * S. ORourke, R.A. Calvo and D. McNamara (to appear) 
 * Visualizing Topic Flow in Students Essays. Journal of Educational Technology and Society.
 * 
 * <b>Status: research</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "flowmap", name = "Flow Map")
public class FlowMapTool extends AbstractTool {
	
	@RequestMapping("/flowmap.htm")
	public ModelAndView handleRequest() throws Exception {
		logger.debug("returning flowmap view");
		Site site = user.getSite();
		Doc doc = user.getDoc();

		// load text document from repository
		TextDocument textDocument = site.getRepository().getTextDocument(doc.tmlId());
		textDocument.load(site.getRepository());
		ParagraphCorpus paragraphCorpus = textDocument.getParagraphCorpus();
		
		//get paragraphs
		List<String> paragraphs = new ArrayList<String>();
		for (int key = 0; key < paragraphCorpus.getPassages().length; key++) {
			TextDocument passage = site.getRepository().getTextDocument(paragraphCorpus.getPassages()[key]);
			paragraphs.add(passage.getContent());
		}

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("paragraphs", paragraphs);

		return new ModelAndView("tool-flowmap", model);
	}
	
	@RequestMapping("/flowmap.png.htm")
	public void generateFlowMap(HttpServletResponse response) throws Exception {
		Doc doc = user.getDoc();
		Site site = user.getSite();

		// load text document from repository
		TextDocument textDocument = site.getRepository().getTextDocument(doc.tmlId());
		textDocument.load(site.getRepository());

		// load sentences
		List<Doc> sentences = new LinkedList<Doc>();
		SentenceCorpus sentenceCorpus = textDocument.getSentenceCorpus();
		for (int key = 0; key < sentenceCorpus.getPassages().length; key++) {
			TextDocument passage = site.getRepository().getTextDocument(sentenceCorpus.getPassages()[key]);
			Doc sentence = new Doc();
			sentence.setPlainText(passage.getContent());
			sentences.add(sentence);
		}
		
		// load paragraphs
		List<Doc> paragraphs = new LinkedList<Doc>();
		ParagraphCorpus paragraphCorpus = textDocument.getParagraphCorpus();
		for (int key = 0; key < paragraphCorpus.getPassages().length; key++) {
			TextDocument passage = site.getRepository().getTextDocument(paragraphCorpus.getPassages()[key]);
			Doc paragraph = new Doc();
			paragraph.setPlainText(passage.getContent());
			paragraphs.add(paragraph);
		}

		if(paragraphs.size() >= 3) {
			LatentSemanticAnalysis nmf = new LatentSemanticAnalysis();
			nmf.setTermWeighting(TermWeighting.LOG_ENTROPY);
			nmf.setDimensionalityReduction(DimensionalityReduction.NMF_ED);
			nmf.setRank(paragraphs.size()-1);
			nmf.setMinTermFrequency(2);
			nmf.setMaxTermWordCount(1);
			nmf.setMaxTerms(999999);
			nmf.train(sentences);
			nmf.project(paragraphs);

			Instances scaledInstances = null;
			MultiDimensionalScalingNR mds = null;
			for(int i=0;i<3;i++) {
				MultiDimensionalScalingNR mds_new = new MultiDimensionalScalingNR();
				mds_new.setMaxIterations(2000);
				Instances newInstances = mds_new.scale(nmf.docInstances());
				if(mds == null || mds_new.error() < mds.error()) {
					mds = mds_new;
					scaledInstances = newInstances;
				}
			}

			ParagraphMapBuilder pmapbuilder = new ParagraphMapBuilder();
			pmapbuilder.buildMap(scaledInstances, paragraphs);
			
			ParagraphMapVis pmapvis = new ParagraphMapVis();
			pmapvis.setMap(pmapbuilder.getMap());
			pmapvis.setSize(500, 500);
			pmapvis.runAction(MapVis.LAYOUT);
			
			while(pmapvis.getAction(MapVis.DRAW).isRunning() || pmapvis.getAction(MapVis.DRAW).isScheduled() || pmapvis.getAction(MapVis.LAYOUT).isRunning() || pmapvis.getAction(MapVis.LAYOUT).isScheduled()) {
				//wait for layout to finish
			}

			Display display = pmapvis.getVisualization().getDisplay(0);
			OutputStream out = response.getOutputStream();
			response.setContentType("image/png");
			display.saveImage(out, "png", 1.0);
			out.flush();
			out.close();	
		}
	}
}
