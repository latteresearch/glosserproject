/*******************************************************************************
 * Copyright 2010, 2011. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.app.site.Site;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tml.corpus.TextDocument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This tool implements AQG by Ming Liu
 * 
 * <b>Status: research</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "question", name = "Question")
public class QuestionTool extends AbstractTool {
	@RequestMapping("/question.htm")
	public ModelAndView handleRequest() throws Exception {
        logger.debug("returning question view");
        Site site = user.getSite();
        Doc doc = user.getDoc();

		// load text document from repository
		TextDocument textDocument = site.getRepository().getTextDocument(doc.tmlId());
		textDocument.load(site.getRepository());
		/*
		 * QuestionGenerator ag = new QuestionGenerator(); SentenceCorpus corpus
		 * = new SentenceCorpus(textDocument);
		 * corpus.getParameters().setCalculateSemanticSpace(false);
		 * corpus.getParameters().setTermSelectionCriterion(TermSelection.TF);
		 * corpus.getParameters().setTermSelectionThreshold(0);
		 * ag.setCorpus(corpus); ag.start(); QuestionResult ar =
		 * ag.getQuestionresult();
		 */

		// generate questions
		ArrayList<String> questions = new ArrayList<String>();
		// questions.addAll(ar.getQuestions());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("questions", questions);

		return new ModelAndView("tool-question", model);
	}

}
