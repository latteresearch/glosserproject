/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.app.doc.DocStatistics;
import glosser.app.site.Site;
import glosser.web.util.HtmlUtil;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.Span;
import org.htmlparser.util.NodeList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tml.corpus.TextDocument;
import tml.vectorspace.operations.LastPassage;
import tml.vectorspace.operations.PassageExtractionSummarization;
import tml.vectorspace.operations.results.PassageExtractionSummarizationResult;

import java.util.HashMap;
import java.util.Map;

/**
 * This tool tries to show 3 key paragraphs on the left and the conclusions on the right.
 * 
 * The 'conclusions' are really just the last paragraph.
 * This Tool is really only useful in a traditional (e.g. 5 paragraph) writing activity.
 * 
 * <b>Status: production</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "structure", name = "Structure")
public class StructureTool extends AbstractTool {
	@RequestMapping("/structure.htm")
	public ModelAndView handleRequest() throws Exception {
        logger.debug("returning structure view");
        Site site = user.getSite();
        Doc doc = user.getDoc();
		DocStatistics docStats = new DocStatistics(doc);

		// get last paragraph
		TextDocument textDocument = site.getRepository().getTextDocument(doc.tmlId());
		textDocument.load(site.getRepository());
		LastPassage lastParagraphOperation = new LastPassage();
		lastParagraphOperation.setCorpus(textDocument.getParagraphCorpus());
		lastParagraphOperation.start();
		String lastParagraphText = lastParagraphOperation.getResults().get(0).getPassage();
		Doc lastParagraph = docStats.getOriginalSegment(lastParagraphText);

		// get key sentences
		int maxResults = 3;
		HashMap<String, Doc> keySentences = new HashMap<String, Doc>();
		PassageExtractionSummarization keySentencesOperation = new PassageExtractionSummarization();
		keySentencesOperation.setCorpus(textDocument.getSentenceCorpus());
		keySentencesOperation.setMaxResults(99999);
		keySentencesOperation.start();

		for (PassageExtractionSummarizationResult result : keySentencesOperation.getResults()) {
			String sentenceText = result.getTextPassageContent();
			if (!lastParagraphText.contains(sentenceText) && !keySentences.containsKey(sentenceText)) {
				Doc sentence = docStats.getOriginalSegment(sentenceText);
				keySentences.put(sentenceText, sentence);
			}

			if (keySentences.size() == maxResults) {
				break;
			}
		}

		// create html text
		String text = doc.getPlainText();
		String html = doc.getContent();
		String htmlGloss = HtmlUtil.mergeHtmlWithPlainText(text, html);

		// replace key sentences
		int i = 1;
		for (String sentenceText : keySentences.keySet()) {
			if (htmlGloss.contains(sentenceText)) {
				// sentence stats
				Doc keySentence = docStats.getOriginalSegment(sentenceText);

				// create image tag
				ImageTag img = new ImageTag();
				img.setAttribute("src", "images/icon-" + i + ".gif");
				img.setEmptyXmlTag(true);

				// create span tag
				Span span = new Span();
				span.setAttribute("class", "text-highlight");
				span.setAttribute("title", String.format("Revision %s, %s, %s", keySentence.getRevision(), keySentence.getAuthor(), keySentence.getDate()));
				NodeList children = new NodeList();
				children.add(img);
				children.add(new TextNode(sentenceText));
				span.setChildren(children);
				Span endSpan = new Span();
				endSpan.setTagName("/" + endSpan.getTagName());
				span.setEndTag(endSpan);

				// update html gloss
				htmlGloss = htmlGloss.substring(0, htmlGloss.indexOf(sentenceText)) + span.toHtml()
						+ htmlGloss.substring(htmlGloss.indexOf(sentenceText) + sentenceText.length());
				i++;
			}
		}

		// replace last paragraph
		if (htmlGloss.contains(lastParagraph.getPlainText())) {
			// create image tag
			ImageTag img = new ImageTag();
			img.setAttribute("src", "images/icon-arrow.gif");
			img.setEmptyXmlTag(true);

			// create span tag
			Span span = new Span();
			span.setAttribute("class", "text-highlight");
			span.setAttribute("title", String.format("Revision %s, %s, %s", lastParagraph.getRevision(), lastParagraph.getAuthor(), lastParagraph.getDate()));
			NodeList children = new NodeList();
			children.add(img);
			children.add(new TextNode(lastParagraphText));
			span.setChildren(children);
			Span endSpan = new Span();
			endSpan.setTagName("/" + endSpan.getTagName());
			span.setEndTag(endSpan);

			// update html gloss
			htmlGloss = htmlGloss.substring(0, htmlGloss.lastIndexOf(lastParagraphText)) + span.toHtml();
		}

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("keySentences", keySentences.values());
		model.put("lastParagraph", lastParagraph);
		model.put("htmlGloss", htmlGloss);

		return new ModelAndView("tool-structure", model);
	}

}
