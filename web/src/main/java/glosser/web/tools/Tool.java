/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import org.springframework.stereotype.Controller;

/**
 * The tool interface is an annotation for adding meta data to Tool classes, which can be read by Glosser at runtime. 
 * 
 * At the moment the @Tool annotation only holds the tool name, but the plan is to add other information about the tool, 
 * such as a set of default questions, default instructions, what TML preprocessing it requires (e.g., PENN tree).
 * @author rafa
 *
 */
@Controller
public @interface Tool {
    String id();
    String name();
}
