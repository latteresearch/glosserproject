/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * This tool implements Concept Map Miner by Jorge Villalon
 * 
 * Discussed in detail in:
 * J. Villalon, R.A. Calvo (to appear) Concept maps as cognitive visualizations of writing assignments. 
 * Journal of Educational Technology and Society.
 * <p>
 * <b>Status: research</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "conceptmap", name = "Concept Map")
public class ConceptMapTool extends AbstractTool {
	@RequestMapping("/conceptmap.htm")
	public ModelAndView handleRequest() throws Exception {
		logger.debug("returning conceptmap view");

		// load text document from repository
		//Repository repository  = user.getSite().getRepository();
		//TextDocument textDocument = repository.getTextDocument(user.getDoc().tmlId());
		//textDocument.load(repository);

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("cmap", "/cmaps/" + user.getDoc().getTitle() + ".png");

		return new ModelAndView("tool-conceptmap", model);
	}

}
