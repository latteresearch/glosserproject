/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.web.util.HtmlUtil;
import datalib.doc.DegreeCentrality;
import datalib.doc.Keyterm;
import datalib.doc.KeytermExtractor;
import docmaps.maps.keyterm.KeytermMapBuilder;
import docmaps.maps.keyterm.data.KeytermMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Provides a visualization of topics and relationships.
 * 
 * Uses MIT's Simile applet.
 * {@link http://simile.mit.edu/}
 * 
 * <b>Status: research</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "topicsmap", name = "Topics Map")
public class TopicsMapTool extends AbstractTool {
	private KeytermExtractor keytermExtractor;
	private int maxEdges = 32;
	private int maxNodes = 20;

	@RequestMapping("/topicsmap.xml.htm")
	public void handleRequest(String graphml, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Doc doc = user.getDoc();

		// build term map
		Collection<Keyterm> keyterms = keytermExtractor.extractKeyterms(doc);
		DegreeCentrality keytermRanker = new DegreeCentrality();
		keytermRanker.setMaxTerms(maxNodes);
		Collection<Keyterm> topKeyterms = keytermRanker.rank(doc, keyterms);

		KeytermMapper keytermMapper = new KeytermMapper();
		keytermMapper.map(doc, topKeyterms, doc.getRevisions());

		logger.debug("returning graphml");
		KeytermMapBuilder mapBuilder = new KeytermMapBuilder();
		mapBuilder.addMap(keytermMapper);
		mapBuilder.filterEdges(maxEdges);
		response.setContentType("text/xml");
		OutputStream out = response.getOutputStream();
		mapBuilder.writeGraphML(out);
		out.flush();
		out.close();
	}

	@RequestMapping("/topicsmap.htm")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug("returning topicmap view");
        Doc doc = user.getDoc();

		// build term map
		Collection<Keyterm> keyterms = keytermExtractor.extractKeyterms(doc);
		DegreeCentrality keytermRanker = new DegreeCentrality();
		keytermRanker.setMaxTerms(maxNodes);
		Collection<Keyterm> topKeyterms = keytermRanker.rank(doc, keyterms);

		// create html text
		String htmlGloss = HtmlUtil.mergeHtmlWithPlainText(doc.getPlainText(), doc.getContent());
		ArrayList<String> topTerms = new ArrayList<String>();
		for (Keyterm topKeyterm : topKeyterms) {
			for (String term : topKeyterm.terms()) {
				topTerms.add(term);
			}
		}

		htmlGloss = HtmlUtil.highlightKeywords(htmlGloss, topTerms);
		String codebaseUrl = StringUtils.substringBeforeLast(request.getRequestURL().toString(), "/") + "/applets/";
		String graphmlUrl = StringUtils.substringBeforeLast(request.getRequestURL().toString(), "/") + "/topicsmap.xml.htm";

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("codebaseUrl", codebaseUrl);
		model.put("graphmlUrl", graphmlUrl);
		model.put("htmlGloss", htmlGloss);

		return new ModelAndView("tool-topicsmap", model);
	}

	public KeytermExtractor getKeytermExtractor() {
		return keytermExtractor;
	}

	@Autowired
	public void setKeytermExtractor(KeytermExtractor keytermExtractor) {
		this.keytermExtractor = keytermExtractor;
	}

	public int getMaxEdges() {
		return maxEdges;
	}

	public void setMaxEdges(int maxEdges) {
		this.maxEdges = maxEdges;
	}

	public int getMaxNodes() {
		return maxNodes;
	}

	public void setMaxNodes(int maxNodes) {
		this.maxNodes = maxNodes;
	}
}
