/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import glosser.app.doc.DocStatistics;
import glosser.app.site.Site;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tml.corpus.TextDocument;
import tml.vectorspace.operations.PassageClusteringLingo;
import tml.vectorspace.operations.results.PassageClusteringLingoResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Shows a Table with key topics, the sentences and the user who wrote each sentence
 * 
 * Eventually this could show compliance with topic for the particular assignment. 
 * But this requires specific values for Activity to be set and the there is not functionality for this.
 * 
 * <b>Status: production</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "topics", name="Topics")
public class TopicsTool extends AbstractTool {
	@RequestMapping("/topics.htm")
	public ModelAndView handleRequest() throws Exception {
        logger.debug("returning topics view");
        Site site = user.getSite();
        Doc doc = user.getDoc();
		DocStatistics docStats = new DocStatistics(doc);

		TextDocument textDocument = site.getRepository().getTextDocument(doc.tmlId());
		textDocument.load(site.getRepository());
		PassageClusteringLingo passageClusteringLingo = new PassageClusteringLingo();
		passageClusteringLingo.setCorpus(textDocument.getSentenceCorpus());
		passageClusteringLingo.start();
		List<PassageClusteringLingoResult> topicResults = passageClusteringLingo.getResults();

		HashMap<String, Doc> topicSentences = new HashMap<String, Doc>();
		HashMap<String, String> topicAuthors = new HashMap<String, String>();
		for (PassageClusteringLingoResult result : topicResults) {
			if (!result.getClusterPhrase().equals("Table")) // ignore Table
			// topic
			{
				HashMap<String, Integer> authors = new HashMap<String, Integer>();
				for (String sentenceId : result.getDocuments()) {
					String sentenceText = passageClusteringLingo.getRepository().getTextDocument(sentenceId).getContent();
					Doc sentence = docStats.getOriginalSegment(sentenceText);
					topicSentences.put(sentenceId, sentence);

					if (authors.containsKey(sentence.getAuthor())) {
						authors.put(sentence.getAuthor(), authors.get(sentence.getAuthor()) + 1);
					} else {
						authors.put(sentence.getAuthor(), 1);
					}
				}
				String topicAuthor = new String();

				int authorCount = 0;
				for (String author : authors.keySet()) {
					if (authors.get(author) > authorCount) {
						topicAuthor = author;
						authorCount = authors.get(author);
					} else if (authors.get(author) == authorCount) {
						topicAuthor = topicAuthor + ", " + author;
					}
				}
				topicAuthors.put(result.getClusterPhrase(), topicAuthor);
			}
		}

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("topicResults", topicResults);
		model.put("topicAuthors", topicAuthors);
		model.put("topicSentences", topicSentences);

		return new ModelAndView("tool-topics", model);
	}
}
