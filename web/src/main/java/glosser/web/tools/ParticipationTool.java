/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.web.tools;

import glosser.app.doc.Doc;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;
/**
 * Group participation view
 * 
 * <b>Status: production</b>
 * 
 * @author rafa
 *
 */
@Tool(id = "participation", name = "Participation")
public class ParticipationTool extends AbstractTool {
	@RequestMapping("/participation.htm")
	public ModelAndView handleRequest() throws Exception {
        logger.debug("returning participation view");
        Doc doc = user.getDoc();

		// calculate user revision stats
		HashMap<String, Integer> revisions = new HashMap<String, Integer>();
		for (Doc revision : doc.getRevisions()) {
			String author = revision.getAuthor();
			if (revisions.containsKey(author)) {
				revisions.put(author, revisions.get(author) + 1);
			} else {
				revisions.put(author, 1);
			}
		}

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("revisions", doc.getRevisions());
		model.put("revisionStats", revisions);

		return new ModelAndView("tool-participation", model);
	}

}
