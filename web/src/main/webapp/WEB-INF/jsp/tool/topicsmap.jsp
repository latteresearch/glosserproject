<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
		<span id="applet-loading" style="background:#FFFFCC; color:#000000; font-weight:bold;"><spring:message code="tool.topicsmap.loading"/></span>
		<div class="applet">
			<applet name="KeytermMapApplet" width="665px" height="520px"
					code="docmaps.maps.keyterm.gui.KeytermMapApplet.class" 
					archive="docmaps-maps.jar,docmaps-vislib.jar,docmaps-datalib.jar,prefuse.jar"
					codebase="<c:out value="${codebaseUrl}"/>"
					scriptable="true" mayscript="true">
				<PARAM NAME="datafile" VALUE="<c:out value="${graphmlUrl}"/>"/>
				<PARAM NAME="width" VALUE="665"/>
				<PARAM NAME="height" VALUE="520"/>
				If you can read this text, the applet is not working. Perhaps you don't
				have the <a href="http://java.com">Java</a> 1.5 (or later) web plug-in installed?
			</applet>
		</div> <!-- end applet -->
		<script type="text/javascript">
			function clearElement(id) {document.getElementById(id).innerHTML = '';}
       		setTimeout("clearElement('applet-loading')", 5000);
		</script>
	</div> <!-- end document-text -->
</div><!-- end document -->
<br/><br/>
<!-- Document -->
<div class="document">

	<div class="document-text">
	<br/><br/>
		<c:out escapeXml="false" value="${htmlGloss}"/>
