<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
	<table>
		<tr>
			<td style="border-style:hidden" width="50%">
				<table>
					<c:set var="count" value="1"/>
					<c:forEach items="${keySentences}" var="sentence">
						<tr>
							<td  style="border-width:0px;" valign="top"><img src="images/icon-<c:out value="${count}"/>.gif" /></td>
							<td  style="border-width:0px;" valign="top">
							<p class="text" title="<spring:message code="document.revision"/> <c:out value="${sentence.revision}"/>, <c:out value="${sentence.author}" escapeXml="false"/>, <c:out value="${sentence.date}"/>">
								<c:out value="${sentence.plainText}"/>
							</p></td>
						</tr>
						<c:set var="count" value="${count + 1}"/>
					</c:forEach>
				</table>
			</td>
			<td style="border-style:hidden" valign="center"><img src="images/icon-arrow.gif" /></td>
			<td style="border-style:hidden" valign="center">
				<p class="text" title="<spring:message code="document.revision"/> <c:out value="${lastParagraph.revision}"/>, <c:out value="${lastParagraph.author}" escapeXml="false"/>, <c:out value="${lastParagraph.date}"/>">
					<c:out value="${lastParagraph.plainText}"/>
				</p>
			</td>
		</tr>
	</table>
	</div> <!-- end document-text -->
</div><!-- end document -->
<br/><br/>
<!-- Document -->
<div class="document">
	
	<div class="document-top">
		<br/>
	</div>

	<div class="document-text">
	<br/><br/>
		<c:out escapeXml="false" value="${htmlGloss}"/>
