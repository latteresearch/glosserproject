<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<h2>Basic Statistics</h2>
<table>
	<tr>
		<th><spring:message code="tool.participation.stats.author"/></th>
		<th><spring:message code="tool.participation.stats.revisions"/></th>
	</tr>
	<c:forEach items="${revisionStats}" var="revisionStat">
		<tr>
		<td><c:out value="${revisionStat.key}" escapeXml="false"/></td>
		<td><c:out value="${revisionStat.value}"/></td>
		</tr>
	</c:forEach>
</table>
<br/>
<h2>Timeline</h2>
<div id="doc-timeline" style="height: 450px; border: 1px solid #aaa"></div>

<script type="text/javascript">
  	var eventSource = new Timeline.DefaultEventSource();
  	var bandInfos = [
    	Timeline.createBandInfo({
        	eventSource:    eventSource,
        	date:			new Date(<c:out value="${revisions[fn:length(revisions)-1].date.time}" />), 
        	width:          "80%", 
        	intervalUnit:   Timeline.DateTime.WEEK, 
        	intervalPixels: 160
    	}),
    	Timeline.createBandInfo({
        	showEventText:  false,
        	trackHeight:    0.5,
        	trackGap:       0.2,
        	eventSource:    eventSource,
        	date:			new Date(<c:out value="${revisions[fn:length(revisions)-1].date.time}" />),
        	width:          "20%", 
        	intervalUnit:   Timeline.DateTime.MONTH, 
        	intervalPixels: 200
   		})
	];
	
  	bandInfos[1].syncWith = 0;
  	bandInfos[1].highlight = true;
  	
  	var tl = Timeline.create(document.getElementById("doc-timeline"), bandInfos);
	
  	//create events
  	var events = [
  		<c:set var="expectedRevision" value="0"/>
  		<c:forEach var="doc" items="${revisions}">
			<c:if test="${expectedRevision != 0}">,</c:if>new Timeline.DefaultEventSource.Event(<c:out value="${doc.date.time}"/>, null, null, null, null, "<c:if test="${doc.revision != expectedRevision}"><c:out value="${expectedRevision}-"/></c:if><c:out value="${doc.revision}"/>", "<spring:message code="document.description" arguments="${doc.revision},${doc.author},${doc.date}" htmlEscape="false"/>", null, null, null, null, null)
			<c:set var="expectedRevision" value="${doc.revision + 1}"/>
		</c:forEach>
  	];

   	eventSource.addMany(events); 	  	
  	tl.layout();

</script>

