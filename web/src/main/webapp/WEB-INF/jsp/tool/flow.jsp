<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>

<script type="text/javascript">
	addScriptToHead("js/flow.js");
</script>
<script type="text/javascript" src="js/flow.js"></script>

<table id="sliders">
	<tr>
		<th><span class="icon-distance"></span></th>
		<th><strong><spring:message code="tool.flow.min"/></strong></th>
		<th style="width:100%;"><div id="distanceSlider"></div></th>
		<th><strong><spring:message code="tool.flow.max"/></strong></th>
	</tr>
	<!--
	<tr>
		<th><span class="icon-readability"></span></th>
		<th><strong><spring:message code="tool.flow.min"/></strong></th>
		<th style="width:100%;><div id="readabilitySlider"></div></th>
		<th><strong><spring:message code="tool.flow.max"/></strong></th>
	</tr>  
	-->
</table>
<br/><br/>
<c:out escapeXml="false" value="${htmlGloss}"/>

<script type="text/javascript"> 

	$(document).ready(function(){
    	$("#distanceSlider").slider(
			{
	        	min: 0,
	        	max: 1,
				step: 0.01,
	    		slide: function(event, ui) {
	    			clear("distance");
	    			//clear("readability");
	    			highlight("distance",$("#distanceSlider").slider('value')*<c:out value="${maxDistance}"/>);
	    			//highlight("readability",$("#readabilitySlider").slider('value')*<c:out value="${maxReadability}"/>);
	        	}
        	}
        );
    	//$("#readabilitySlider").slider(
    	//	{
        //    	min: 0,
        //    	max: 1,
	 	//		step: 0.01,
        //		slide: function(event, ui) {
        //			clear("distance");
        //			clear("readability");
        //			highlight("distance",$("#distanceSlider").slider('value')*<c:out value="${maxDistance}"/>);
        //			highlight("readability",$("#readabilitySlider".slider('value')*<c:out value="${maxReadability}"/>);
        //		}
        //	}
        //);
		$("#distanceSlider").slider('value',<c:out value="${distanceThreshold}"/>/<c:out value="${maxDistance}"/>);
		//$("#readabilitySlider").slider('value',<c:out value="${readabilityThreshold}"/>/<c:out value="${maxReadability}"/>);

	});
</script>
