<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<table>
<tr><th><spring:message code="tool.topics.table.topic"/></th><th><spring:message code="tool.topics.table.sentences"/></th><th><spring:message code="tool.topics.table.authors"/></th></tr>
<c:forEach items="${topicResults}" var="topicResult">
	<tr>
		<td>
			<c:out value="${topicResult.clusterPhrase}"/>
		</td>
		<td>
			<ul>
			<c:forEach items="${topicResult.documents}" var="sentenceId">
				<c:set var="sentence" value="${topicSentences[sentenceId]}"/>
				<li class="text" title="<spring:message code="document.revision-long" arguments="${sentence.revision},${sentence.author},${sentence.date}" htmlEscape="false"/>">
			 		<c:out value="${sentence.plainText}"/>
			 	</li>
			</c:forEach>
			</ul>
		</td>
		<td>
			<c:out value="${topicAuthors[topicResult.clusterPhrase]}" escapeXml="false"/>	
		</td>
	</tr>
</c:forEach>
</table>
