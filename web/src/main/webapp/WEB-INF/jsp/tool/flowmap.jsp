<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
	<br/>
	<div style="text-align: center"><img src="flowmap.png.htm" alt="Flow Map" /></div>
	</div> <!-- end document-text -->
</div><!-- end document -->
<br/><br/>
<!-- Document -->
<div class="document">

	<div class="document-text">
	<br/><br/>
		<table>
		<c:set var="count" value="1"/>
		<c:forEach items="${paragraphs}" var="paragraph">
			<tr>
				<th><h2><c:out value="${count}"/></h2></th>
				<td><p><c:out value="${paragraph}"/></p></td>
			</tr>
			<c:set var="count" value="${count + 1}"/>
		</c:forEach>
		</table>
