<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<table width="100%" style="margin-left:auto;">
<tr><th><spring:message code="doclist.table.name"/></th><th width="100%"></th></tr>

<c:forEach items="${docs}" var="doc">
	<tr>
		<c:url var="glossUrl" value="home.htm"><c:param name="docId" value="${doc.id}"/></c:url>
		<c:choose>
			<c:when test="${doc.indexed}">			
				<td style="white-space: nowrap;"><a href="<c:out value="${glossUrl}"/>"><span id="glossicon-<c:out value="${doc.id}"/>" class="icon-gloss" title="<spring:message code="tooltip.gloss"/> - <spring:message code="document.revision"/> <c:out value="${doc.revision}"/>, <c:out value="${doc.author}" escapeXml="false"/>, <c:out value="${doc.date}"/>"></span></a>
				<a id="glossurl-<c:out value="${doc.id}"/>" href="<c:out value="${glossUrl}"/>" title="<spring:message code="tooltip.gloss"/> - <spring:message code="document.revision"/> <c:out value="${doc.revision}"/>, <c:out value="${doc.author}" escapeXml="false"/>, <c:out value="${doc.date}"/>"><c:out value="${doc.title}"/></a></td>
			</c:when>
			<c:otherwise>
				<td style="white-space: nowrap;"><a href="<c:out value="${glossUrl}"/>"><span id="glossicon-<c:out value="${doc.id}"/>" class="icon-nogloss"  title="<spring:message code="tooltip.gloss"/> - Never"></span></a>
				<a id="glossurl-<c:out value="${doc.id}"/>" href="<c:out value="${glossUrl}"/>" title="<spring:message code="tooltip.gloss"/> - Never"><c:out value="${doc.title}"/></a></td>
			</c:otherwise>
		</c:choose>
		
		<td>
		<span id="reload-<c:out value="${doc.id}"/>">
			<c:if test="${site.autoHarvest}">
				<a href="#" onClick="javascript:queueDoc('<c:out value="${doc.id}"/>')"><img src="images/reload.gif" title="<spring:message code="tooltip.reload"/>"/></a>	
			</c:if>
		</span>	
		</td>
	</tr>				
</c:forEach>
</table>

<script type="text/javascript">
	<c:forEach items="${docs}" var="doc">
		<c:if test="${!empty doc.indexed && !doc.indexed}">	
			setIndexMessage('<c:out value="${doc.id}"/>');
			indexCountdown('<c:out value="${doc.id}"/>');
		</c:if>
	</c:forEach>
</script>
