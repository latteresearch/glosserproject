<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<tiles:importAttribute name="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title><spring:message code="title"/> - <spring:message code="${page}.heading"/></title>
	<link type="image/gif" rel="icon" href="images/icon-g-green.gif"></link>
	<link type="text/css" rel="stylesheet" href="css/glosser.css"></link>
</head>	
	<body>	
	<!-- Header and navigation -->
	<div id="header">
		<span id="logo"><img src="images/logo-grey.gif" width="232" height="52" alt="Glosser" /></span>	
	</div>	

	<div id="main">
		
		<div id="green-bar">&nbsp;</div>
	
		<div class="document">
			<div class="document-text">
				<tiles:insertAttribute name="content"/>
			</div>	
		</div><!-- end document -->
		
		<div id="footer">
			Glosser � copyright 2010, The University of Sydney<br />
			Funded by <a href="http://www.usyd.edu.au/learning/quality/ties.shtml">TIES</a>. Developed by <a href="http://weg.ee.usyd.edu.au">LATTE</a> - Fac. Engineering and the <a href="http://www.usyd.edu.au/stuserv/learning_centre">Learning Centre</a><br/>
		</div>
				
	</div><!-- end main -->
	
	
	<!-- Google Analytics -->
	<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		var pageTracker = _gat._getTracker("YOUR-TRACKER-ID");
		pageTracker._trackPageview();
	</script>			
</body>
</html>
