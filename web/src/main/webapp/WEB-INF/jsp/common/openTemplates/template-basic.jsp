<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<tiles:importAttribute name="page"/>
<tiles:importAttribute name="site" /> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title><spring:message code="title"/> - <spring:message code="${page}.heading"/></title>
	<link type="image/gif" rel="icon" href="images/icon-g-green.gif"></link>
	<link type="text/css" rel="stylesheet" href="css/glosser.css"></link>
	<link type="text/css"  href="http://jquery-ui.googlecode.com/svn/tags/latest/themes/base/jquery.ui.all.css"  rel="stylesheet" />
	<script type="text/javascript" src="js/indexer.js"></script>
	<script type="text/javascript" src="js/timer.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="http://static.simile.mit.edu/timeline/api/timeline-api.js?bundle=true"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="dwr/interface/IndexerQueuer.js"></script>
	<script type="text/javascript" src="dwr/engine.js"></script>
	<script type="text/javascript" src="dwr/util.js"></script>
</head>	
<body>
	<!-- Header and navigation 
	<div id="nav-top">
		<span id="nav-global">
			<a href="/">Home</a>
			&nbsp;&nbsp;<a href="/writingclearly">Writing Clearly</a>
			&nbsp;&nbsp;<a class="selected" href="/glosser">Feedback Tools</a>
			&nbsp;&nbsp;<a href="/learnmore.html">Further Learning</a>
		</span>
	 	<span id="nav-local">
			<a href="javascript:openWindow('help.htm');"><spring:message code="header.link.help"/></a>
			&nbsp;|&nbsp;
			<a href="logout.htm"><spring:message code="header.link.logout"/></a> 
		</span>
		&nbsp;
	</div>
	-->

	<div id="header">
		<span id="logo"><img src="images/logo-green.gif" width="232" height="52" alt="Glosser" /></span>	
	</div>
		
	<div id="navigation">
		<ul>		
			<li ><a href="http://glosserproject.org">Home</a></li>
			<c:forEach var="tool" items="${site.tools}">
                            <c:set var="toolpage" value="tool.${tool}" />
                            <c:choose>
					<c:when test="${page == toolpage}"><li class="selected"><a href=""><spring:message code="tool.${tool}.heading"/></a></li></c:when>
					<c:otherwise><li><a href="<c:url value="${tool}.htm"/>"><spring:message code="tool.${tool}.heading"/></a></li></c:otherwise>	
				</c:choose>
			</c:forEach>					
		</ul>
	</div>	

	<div id="main">
		
		<div id="green-bar">&nbsp;</div>
	
		<div class="instructions">
			<ul><li><spring:message code="${page}.instructions"/></li></ul>
		</div><!-- end instructions -->	

		<div class="document">
			<div class="document-text">
				<tiles:insertAttribute name="content"/>	
			</div>	
		</div><!-- end document -->
		
		<div id="footer">
			<a href="javascript:openWindow('credits.htm');"><spring:message code="footer.link.credits"/></a>
			<br/>
			Glosser � copyright 2010, The University of Sydney<br />
			Funded by <a href="http://www.usyd.edu.au/learning/quality/ties.shtml">TIES</a>. Developed by <a href="http://weg.ee.usyd.edu.au">LATTE</a> - Fac. Engineering and the <a href="http://www.usyd.edu.au/stuserv/learning_centre">Learning Centre</a><br/>
		</div>		
		
	</div><!-- end main -->	

	
	<!-- Google Analytics -->
	<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		var pageTracker = _gat._getTracker("YOUR-TRACKER-ID");
		pageTracker._trackPageview();
	</script>				
</body>
</html>