<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<h2>Glosser</h2>
<p>Watch the video below for a quick introduction to Glosser.</p>
<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/6Tc6jLJCBrM&hl=en_US&fs=1&rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/6Tc6jLJCBrM&hl=en_US&fs=1&rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>
<br/><br/>
<p>Glosser is intended to facilitate the review of argumentative essays. It does this by supplying useful information about the essay, which is intended to make the evaluation of the essay an easier task.</p>
<p>The programme carries a number of assumptions about essays. It assumes that essays should be an argument, asserting a particular point of view, and should back up this argument with well-thought out reasoning and evidence. It should neither be a re-stating of official, academic knowledge, nor should it be a simple, unsupported opinion. Essays should be comprehensive, while remaining relevant, when discussing a topic. These assumptions are widely held, both in academic research and writing departments.</p>
<p>The second assumption is that the programme cannot decide whether the evidence and argumentation is reasonable or not. That will always be something that must be decided by another reader. However, the programme can help this evaluation by giving you information about the sort of statements the essay is making, the ideas and keywords it includes, and the way these are organised.</p>
<p>The questions at the top of each page are provided to help the reader focus his/her evaluation of the essay. They are intended to make sure that you cover all aspects of a good argument.</p>
<p>The reader should use the programme accordingly, and use the information provided by the programme to help answer the questions. Overall, however, he/she should focus on making the essay more structured, comprehensive, and readable.</p>
<br/>
<!--
<h2>Structure</h2>
<p>This section reproduces the most important statements in the essay, alongside the conclusion. It should help answer the questions, which focus on evidence, structure and reasoning.</p>
<br/>
-->
<h2>Flow</h2>
<p>This section indicates if two paragraphs are incoherent. This is measured purely on the similarity of the subject matter, so ignores other ways of producing flow, such as linking words (however, first, secondly, etc). If two paragraphs are in bold, it means that there has been a large jump in the subject matter. If this makes the essay difficult to read or understand, then it should be changed.</p>
<br/>
<h2>Topics</h2>
<p>This section attempts to list the ideas that have been discussed in the essay, and the sentences that contribute to those ideas. It is an opportunity to consider the comprehensiveness of the essay, whether it has included everything it should to cover the topic well, and excluded anything isn' relevant.</p>
<br/>
<h2>Topics Map</h2>
<p>This section reproduces the key words as they are used in relationship to each other. If two keywords are used in the same sentence, they are linked in the network representation. A good network should link a variety of keywords, not just the central word to the others. That is, a good essay should be discussing the concepts from as many angles as possible, comparing them with other concepts, and not simply stating them on their own.</p>
<br/>
<h2>Participation</h2>
<p>This section is designed to support collaborative work. If you're writing in a group, this section can help you understand how each group member is participating in the writing process.</p>
