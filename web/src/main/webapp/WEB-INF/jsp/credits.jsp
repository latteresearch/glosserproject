<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 -->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<h2>People</h2>
<p>R.A. Calvo, S. O'Rourke,  B. Paltridge, P. Reimann, J. Villalon, J. Jones, P. O'Carroll</p>

<h2>Grants</h2>
<ul>
	<li>Reimann, P; Calvo, RA and Yacef K. "Comprehensive support for collaborative writing: visualising argument, text and process structures". (2009-2012). Australian Research Council - Discovery Project.</li>
	<li>Calvo, RA; Jones, J, Drury, H Airey, D and See, H "Writing for Engineering Disciplines: Supporting the Development of Student Writing across Curricula" (2009). Teaching Improvement and Equipment Fund - University of Sydney</li>
	<li>Reimann, P; Calvo R.A. and Paltridge B.- "Using machine Learning and automated document analysis methods to support English composition training" (2006-2008). Australian Research Council - Discovery Project.</li>
</ul>
