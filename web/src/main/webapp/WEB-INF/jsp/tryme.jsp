<!-- Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
   http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 - Contributors
 Stephen O'Rourke
 Jorge Villalon (CMM)
 Ming Liu (AQG)
 Rafael A. Calvo
 Marco Garcia
 Daniel Schragl


 This Website allows to have any GoogleDoc glossed by Glosser without requiring a Glosser-, Unikey-, or Tomcat-Account.
   
   -> THIS WEBSITE LETS USER INPUT a docID and then calls Glosser siteAuth.htm to gloss the document. It only works
      without authentication, if LoginRequestInterceptor.preHandle(...) always returns true! Otherwise requests are
      redirected to login.
  
   
   ADDITIONALLY, THIS NEEDS TO BE IN THE DB (and harvesterPassword has to be set to the password of the Google account
   of harvesterUsername):
    - a glosser instance with siteId=6 with the following settings (in glosser.Site):
+----+-------------+-------------------+---------------+-------------------------------------------+-------------+--------------+-------------------------------------------------+--------------+
| id | autoHarvest | harvesterPassword | harvesterType | harvesterUsername                         | indexPeriod | name         | referringUrl                                    | title        |
+----+-------------+-------------------+---------------+-------------------------------------------+-------------+--------------+-------------------------------------------------+--------------+
...
|  6 |             | ****************  | google        | assignment.tracker@iwrite.eng.usyd.edu.au |       15000 | TryMeGlosser | http://docs.google.com                          | TryMeGlosser |
...
    - a schema glosser_TryMeGlosser
    - an empty table glosser_TryMeGlosser.Doc
       use glosser_TryMeGlosser;
       SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
       CREATE TABLE IF NOT EXISTS `Doc` (   `id` varchar(255) NOT NULL,   `revision` int(11) NOT NULL,   `owner` varchar(255) DEFAULT NULL,   `author` varchar(255) DEFAULT NULL,   `modified` datetime DEFAULT NULL,   `indexed` bit(1) DEFAULT NULL,   `title` varchar(255) DEFAULT NULL,   `content` longtext,   PRIMARY KEY (`id`,`revision`) ) ENGINE=MyISAM DEFAULT CHARSET=latin1;  
-->
<%@ include file="/WEB-INF/jsp/common/include.jsp" %>
<h2>Glosser - Try Me</h2>
    <p><p/>
    <h1><a>Try Glosser Without Account:</a></h1>
    <form id="form_330116" class="appnitro"  method="get" action="siteauth.htm" -target="_blank">
    	<div class="form_description">
            <p>This page let's you execute Glosser with any GoogleDoc document, that is shared with the world or with user assignment.tracker@iWrite.eng.usyd.edu.au. Just paste the GoogleDocID of the document to gloss below and click the button 'Submit'. (The Document ID has to start with 'document:'.)</p>
        </div>
        <ul >
            <li id="li_1" >
                <input type="hidden" name="siteId" value="6" />
                    <label class="description" for="docId">Paste the GoogleDocID of the document to gloss here and click 'Submit': </label>
                <div>
                    <input id="docId" name="docId" class="element text medium" type="text" maxlength="255" value="document:1L9l8ZtgRjie7znagbKPhswuMErwjJA3Twm2b8VfUL7s"/>
                </div>
            </li>

            <li class="buttons">
                <input type="hidden" name="form_id" value="330116" />
                <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
            </li>
        </ul>
    </form>
