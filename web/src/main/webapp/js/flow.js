//#-------------------------------------------------------------------------------
//# Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
//# 
//# Licensed under the Apache License, Version 2.0 (the "License");
//# you may not use this file except in compliance with the License.
//# You may obtain a copy of the License at
//# 
//#   http://www.apache.org/licenses/LICENSE-2.0
//# 
//# Unless required by applicable law or agreed to in writing, software
//# distributed under the License is distributed on an "AS IS" BASIS,
//# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//# See the License for the specific language governing permissions and
//# limitations under the License.
//# - Contributors
//# 	Stephen O'Rourke
//# 	Jorge Villalon (CMM)
//# 	Ming Liu (AQG)
//# 	Rafael A. Calvo
//# 	Marco Garcia
//#-------------------------------------------------------------------------------
var minSentences = 2;

function getElementsByName_iefix(tag, name) 
{
	var elem = document.getElementsByTagName(tag);
	var arr = new Array();
	for(i = 0,iarr = 0; i < elem.length; i++) 
	{
		att = elem[i].getAttribute("name");
		if(att == name) {
			arr[iarr] = elem[i];
			iarr++;
		}
	}
	return arr;
} 

function clear(name)
{
    //clear pargraphs
    var paragraphs = getElementsByName_iefix("span", "paragraph");
    for (var i=0; i<paragraphs.length; i++) {
        paragraphs[i].className = "";
    }   

    var icons = getElementsByName_iefix("span", "icon-" + name);
    for (var i=0; i<paragraphs.length; i++) {
        icons[i].className = "icon-none";
    }
}

function highlight(name, threshold)
{
    var highlight = false;
    var ignoreParagraph = true;
    var paragraphs = getElementsByName_iefix("span", "paragraph");
    var icons = getElementsByName_iefix("span", "icon-" + name);

    for (var i=0; i<paragraphs.length; i++)
    {
       	if(highlight)
        {
            paragraphs[i].className = "text-highlight";
            highlight = false;
        }
        
        if (i < paragraphs.length-1)
        {
	        if(paragraphs[i].getAttribute(name) >= threshold && (paragraphs[i].getAttribute("sentences") >= minSentences && paragraphs[i+1].getAttribute("sentences") >= minSentences))
	        {
	            icons[i].className = "icon-" + name;
	            paragraphs[i].className = "text-highlight";
	            highlight = true;
			}
		}
	}
}
