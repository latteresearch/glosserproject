//#-------------------------------------------------------------------------------
//# Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
//# 
//# Licensed under the Apache License, Version 2.0 (the "License");
//# you may not use this file except in compliance with the License.
//# You may obtain a copy of the License at
//# 
//#   http://www.apache.org/licenses/LICENSE-2.0
//# 
//# Unless required by applicable law or agreed to in writing, software
//# distributed under the License is distributed on an "AS IS" BASIS,
//# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//# See the License for the specific language governing permissions and
//# limitations under the License.
//# - Contributors
//# 	Stephen O'Rourke
//# 	Jorge Villalon (CMM)
//# 	Ming Liu (AQG)
//# 	Rafael A. Calvo
//# 	Marco Garcia
//#-------------------------------------------------------------------------------
//Open browser.
function openWindow(url) {
	window.open(url, null, 'resizable=yes,toolbar=no,scrollbars=yes,menubar=yes,width=1050,height=650'); 
}

function addScriptToHead(url) {
	var script = document.createElement("script");
 	script.type = "text/javascript";
	script.src = url;
	var head = document.getElementsByTagName("head")[0];
	head.appendChild(script);
}
