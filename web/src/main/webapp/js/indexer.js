//#-------------------------------------------------------------------------------
//# Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
//# 
//# Licensed under the Apache License, Version 2.0 (the "License");
//# you may not use this file except in compliance with the License.
//# You may obtain a copy of the License at
//# 
//#   http://www.apache.org/licenses/LICENSE-2.0
//# 
//# Unless required by applicable law or agreed to in writing, software
//# distributed under the License is distributed on an "AS IS" BASIS,
//# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//# See the License for the specific language governing permissions and
//# limitations under the License.
//# - Contributors
//# 	Stephen O'Rourke
//# 	Jorge Villalon (CMM)
//# 	Ming Liu (AQG)
//# 	Rafael A. Calvo
//# 	Marco Garcia
//#-------------------------------------------------------------------------------
//Messages
var indexingMsg 	= "Indexing... please be patient, this could take a few minutes.";
var finishedMsg 	= "Finished indexing.";
var finishedTooltip = "Gloss the document - Latest revision";
var period = 10000;

//Index document
function queueDoc(docId)
{
	setIndexMessage(docId);
	IndexerQueuer.queueDoc(docId, {
	  	callback:function(){indexCountdown(docId);}
	});
} 

//Index message
function setIndexMessage(docId)
{
	dwr.util.setValue("reload-" + docId, indexingMsg);
	document.getElementById("glossicon-" + docId).className = "icon-waitgloss";
} 

//Index countdown
function indexCountdown(docId) 
{ 
	IndexerQueuer.isIndexed(docId,
	{
	  	callback:function(isIndexed) 
	  	{ 	
	  		if(isIndexed) {
	  			dwr.util.setValue("reload-" + docId, finishedMsg);
	  			document.getElementById("glossicon-" + docId).className = "icon-gloss";
	  			document.getElementById("glossicon-" + docId).title = finishedTooltip;
				document.getElementById("glossurl-" + docId).title = finishedTooltip;
	  		} else {
	  			setTimeout(function(){indexCountdown(docId);}, period);
	  		}
		}
	}); 
}
