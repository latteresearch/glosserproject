//#-------------------------------------------------------------------------------
//# Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
//# 
//# Licensed under the Apache License, Version 2.0 (the "License");
//# you may not use this file except in compliance with the License.
//# You may obtain a copy of the License at
//# 
//#   http://www.apache.org/licenses/LICENSE-2.0
//# 
//# Unless required by applicable law or agreed to in writing, software
//# distributed under the License is distributed on an "AS IS" BASIS,
//# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//# See the License for the specific language governing permissions and
//# limitations under the License.
//# - Contributors
//# 	Stephen O'Rourke
//# 	Jorge Villalon (CMM)
//# 	Ming Liu (AQG)
//# 	Rafael A. Calvo
//# 	Marco Garcia
//#-------------------------------------------------------------------------------
var currentRating = 0;

//Set rating.
function rate(rating)
{
	if (currentRating == rating) {
		currentRating--;
	}
	else {
		currentRating = rating;
	}
	
	ele = document.getElementsByName("star");
	for(var i=0; i<ele.length; i++)
	{
		if(i < currentRating) {
			ele[i].setAttribute("src", "images/star-on.png");
		}
		else {
			ele[i].setAttribute("src", "images/star-off.png");
		}
	}
}

//Load rating
function loadRating(page)
{
	Rater.isRated(page, 
	{
	  	callback:function(isRated)
	  	{ 
		 	if (isRated)
			{
				Rater.loadRating(page, rate);
				lockRatingBar();
			} 	
	  	}
  	});	
}

//Save rating;
function saveRating(page)
{
	Rater.saveRating(page, currentRating);
	lockRatingBar();
}

//Lock rating bar.
function lockRatingBar()
{	
	dwr.util.setValue("rate","");
	ele = document.getElementsByName("star");
	for(var i=0; i<ele.length; i++)
	{
		ele[i].setAttribute("onClick", "");
	}
}
