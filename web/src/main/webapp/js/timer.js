var secondsRefresh;
var start; 

function reloadTimer(seconds) 
{ 
	secondsRefresh = seconds;
	start = new Date();
	reloadCountdown();
} 

function reloadCountdown() 
{ 
	secondsRefresh=Math.round(secondsRefresh-1);
	if (secondsRefresh > 0) { 
		document.getElementById("timer").innerHTML = secondsRefresh + "...";
		var timer = setTimeout("reloadCountdown()", 1000); 
	} 
	else {
		document.getElementById("timer").innerHTML = "Reloading..."; 
		window.location.reload(true);
	} 
}
