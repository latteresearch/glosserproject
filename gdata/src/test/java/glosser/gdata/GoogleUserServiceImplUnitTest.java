/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.gdata;

import com.google.gdata.data.appsforyourdomain.provisioning.UserEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import javax.mail.NoSuchProviderException;
import java.io.IOException;
import java.net.MalformedURLException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GoogleUserServiceImplUnitTest extends GoogleServiceTestBase {

    private GoogleUserServiceImpl googleUserServiceImpl;

    @Before
    public void setUp() throws InterruptedException, AuthenticationException, MalformedURLException, NoSuchProviderException {
    	googleUserServiceImpl = new GoogleUserServiceImpl(username, password, domain);
    }

    @Test
    public void testRetrieveUser() throws ServiceException, IOException {
        UserEntry userEntry = googleUserServiceImpl.retrieveUser(StringUtils.substringBefore(username, "@"));
        assertThat(userEntry.getLogin().getUserName(), is(StringUtils.substringBefore(username, "@")));
    }
}
