/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.gdata;

import com.google.gdata.data.docs.*;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.NoSuchProviderException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class GoogleDocsServiceImplUnitTest extends GoogleServiceTestBase {

	private static GoogleDocsServiceImpl googleDocsServiceImpl;

	@BeforeClass
	public static void setUp() throws InterruptedException, AuthenticationException, MalformedURLException, NoSuchProviderException, FileNotFoundException {
		googleDocsServiceImpl = new GoogleDocsServiceImpl(username, password);
	}

	@Test
	public void shouldCreateRetrieveAndDeleteFolder() throws IOException, ServiceException {
		// create folder
		FolderEntry newFolderEntry = googleDocsServiceImpl.createFolder("test folder");
		assertThat(newFolderEntry.getTitle().getPlainText(), is("test folder"));

		// retrieve folder
		FolderEntry folderEntry = googleDocsServiceImpl.getFolder(newFolderEntry.getResourceId());
		assertThat(newFolderEntry.getResourceId(), is(folderEntry.getResourceId()));

		// delete folder
		googleDocsServiceImpl.delete(folderEntry);
		folderEntry = googleDocsServiceImpl.getFolder(folderEntry.getResourceId());
		assertThat(folderEntry.isTrashed(), is(true));
	}

	@Test
	public void shouldCreateRetrieveAndDeletePresentation() throws IOException, ServiceException {
		// create presentation
		PresentationEntry newPresentationEntry = googleDocsServiceImpl.createPresentation("test presentation");
		assertThat(newPresentationEntry.getTitle().getPlainText(), is("test presentation"));

		// retrieve presentation
		PresentationEntry presentationEntry = googleDocsServiceImpl.getPresentation(newPresentationEntry.getResourceId());
		assertThat(presentationEntry.getResourceId(), is(newPresentationEntry.getResourceId()));

		// delete presentation
		googleDocsServiceImpl.delete(presentationEntry);
		presentationEntry = googleDocsServiceImpl.getPresentation(presentationEntry.getResourceId());
		assertThat(presentationEntry.isTrashed(), is(true));
	}

	@Test
	public void shouldCreateRetrieveAndDeleteSpreadsheet() throws IOException, ServiceException {
		// create spreadsheet
		SpreadsheetEntry newSpreadsheetEntry = googleDocsServiceImpl.createSpreadsheet("test spreadsheet");
		assertThat(newSpreadsheetEntry.getTitle().getPlainText(), is("test spreadsheet"));

		// retrieve spreadsheet
		SpreadsheetEntry spreadsheetEntry = googleDocsServiceImpl.getSpreadsheet(newSpreadsheetEntry.getResourceId());
		assertThat(newSpreadsheetEntry.getResourceId(), is(spreadsheetEntry.getResourceId()));

		// delete spreadsheet
		googleDocsServiceImpl.delete(spreadsheetEntry);
		spreadsheetEntry = googleDocsServiceImpl.getSpreadsheet(spreadsheetEntry.getResourceId());
		assertThat(spreadsheetEntry.isTrashed(), is(true));
	}

	@Test
	public void shouldCreateRetrieveCopyDownloadAndDeleteDocument() throws IOException, ServiceException, URISyntaxException  {
		// create document
		DocumentEntry newDocumentEntry = googleDocsServiceImpl.createDocument("test document");
		assertThat(newDocumentEntry.getTitle().getPlainText(), is("test document"));

		// retrieve document
		DocumentEntry documentEntry = googleDocsServiceImpl.getDocument(newDocumentEntry.getResourceId());
		assertThat(newDocumentEntry.getResourceId(), is(documentEntry.getResourceId()));

		// upload document content
		//File file =  new File(GoogleDocsServiceImplUnitTest.class.getResource("/template.html").toURI());
		//documentEntry = googleDocsServiceImpl.uploadDocumentContent(documentEntry, file);		
		
		// copy document
		DocumentEntry copyDocumentEntry = googleDocsServiceImpl.copyDocument("copy test document", documentEntry);		
		assertThat(copyDocumentEntry.getTitle().getPlainText(), is("copy test document"));
		
		// download document revisions
		List<RevisionEntry> revisionEntries = googleDocsServiceImpl.getDocumentRevisions(documentEntry);
		for (RevisionEntry revisionEntry : revisionEntries) {
			String revisionHtml = googleDocsServiceImpl.downloadDocumentHtml(revisionEntry);
			assertThat(revisionHtml, notNullValue());
		}

		// delete document
		googleDocsServiceImpl.delete(documentEntry);
		documentEntry = googleDocsServiceImpl.getDocument(documentEntry.getResourceId());
		assertThat(documentEntry.isTrashed(), is(true));
		
		// delete document copy
		googleDocsServiceImpl.delete(copyDocumentEntry);
		copyDocumentEntry = googleDocsServiceImpl.getDocument(copyDocumentEntry.getResourceId());
		assertThat(copyDocumentEntry.isTrashed(), is(true));
	}
}
