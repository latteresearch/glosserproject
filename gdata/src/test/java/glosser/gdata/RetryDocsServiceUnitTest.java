/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.gdata;

import com.google.gdata.client.GoogleAuthTokenFactory.UserToken;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class RetryDocsServiceUnitTest extends GoogleServiceTestBase {

	@Test
	public void shouldReauthenticate() throws IOException, ServiceException {
		RetryDocsService docsService = new RetryDocsService("Docs Service");
		docsService.setUserCredentials(username, password);

		// reauthenticate service
		String token = ((UserToken)docsService.getAuthTokenFactory().getAuthToken()).getValue();
		docsService.handleException(0, new AuthenticationException("Unauthorized\n" + 
				"<HTML>\n" + 
				"<HEAD>\n" + 
				"<TITLE>Token expired</TITLE>\n" + 
				"</HEAD>\n" + 
				"<BODY BGCOLOR=\"#FFFFFF\" TEXT=\"#000000\">\n" + 
				"<H1>Token expired</H1>\n" + 
				"<H2>Error 401</H2>\n" + 
				"</BODY>\n" + 
				"</HTML>"));
		String newToken = ((UserToken)docsService.getAuthTokenFactory().getAuthToken()).getValue();
		assertThat(newToken, not(token));
	}
}
