/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.gdata;

import com.google.gdata.data.docs.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GoogleSpreadsheetImplUnitTest extends GoogleServiceTestBase {

    private GoogleSpreadsheetServiceImpl googleSpreadsheetServiceImpl;
	private GoogleDocsServiceImpl googleDocsServiceImpl;
	private SpreadsheetEntry newSpreadsheetEntry;

	@Before
	public void setUp() throws Exception {
		googleSpreadsheetServiceImpl = new GoogleSpreadsheetServiceImpl(username, password);
		googleDocsServiceImpl = new GoogleDocsServiceImpl(username, password);
		newSpreadsheetEntry = googleDocsServiceImpl.createSpreadsheet("test spreadsheet");
		googleDocsServiceImpl.uploadSpreadsheetCsv(newSpreadsheetEntry, "property1,property2,");
	}

	@Test
	public void shouldRetrieveAndModifySpreadsheet() throws IOException, ServiceException {
		// retrieve spreadsheet
		List<WorksheetEntry> worksheetEntries = googleSpreadsheetServiceImpl.getSpreadsheetWorksheets(newSpreadsheetEntry);
		assertThat(worksheetEntries.size(), is(1));
		List<ListEntry> listEntries = googleSpreadsheetServiceImpl.getWorksheetRows(worksheetEntries.get(0));
		assertThat(listEntries.size(), is(0));

		// add spreadsheet row
		ListEntry listEntry = new ListEntry();
		listEntry.getCustomElements().setValueLocal("property1", "value1");
		listEntry.getCustomElements().setValueLocal("property2", "value2");
		googleSpreadsheetServiceImpl.addWorksheetRow(worksheetEntries.get(0), listEntry);
		listEntries = googleSpreadsheetServiceImpl.getWorksheetRows(worksheetEntries.get(0));
		assertThat(listEntries.size(), is(1));
	}
	
	@After
	public void cleanUp() throws Exception {
		newSpreadsheetEntry = googleDocsServiceImpl.getSpreadsheet(newSpreadsheetEntry.getResourceId());
		googleDocsServiceImpl.delete(newSpreadsheetEntry);
	}
}
