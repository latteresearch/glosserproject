/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.gdata;

import com.google.gdata.util.AuthenticationException;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.BeforeClass;

import javax.mail.NoSuchProviderException;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.Iterator;

public class GoogleServiceTestBase {

    protected static String username;
    protected static String password;
    protected static String domain;

    @BeforeClass
    public static void loadProperties() throws InterruptedException, AuthenticationException, MalformedURLException, NoSuchProviderException, FileNotFoundException, ConfigurationException {
        Configuration config = new PropertiesConfiguration("glosser.properties");
        for (Iterator<String> keys = config.getKeys(); keys.hasNext();) {
            String key = keys.next();
            System.setProperty(key, config.getString(key));
        }

        username = System.getProperty("glosser.google.username");
        password = System.getProperty("glosser.google.password");
        domain = System.getProperty("glosser.google.domain");
    }
}
