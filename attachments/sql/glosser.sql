/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Database       : glosser

Target Server Type    : MYSQL

Date: 2011-11-21 10:13:15
*/

USE glosser;
SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `Question_Score`
-- ----------------------------
DROP TABLE IF EXISTS `Question_Score`;
CREATE TABLE `Question_Score` (
  `question_id` bigint(20) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK52A6B4F9B3D1BA0` (`id`),
  KEY `FK52A6B4F9F2CD5E05` (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Question_Score
-- ----------------------------

-- ----------------------------
-- Table structure for `Score`
-- ----------------------------
DROP TABLE IF EXISTS `Score`;
CREATE TABLE `Score` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `QualityMeasure` int(11) NOT NULL,
  `grade` int(11) NOT NULL,
  `predicatedProducer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Score
-- ----------------------------

-- ----------------------------
-- Table structure for `Site`
-- ----------------------------
DROP TABLE IF EXISTS `Site`;
CREATE TABLE `Site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `autoHarvest` bit(1) NOT NULL,
  `harvesterPassword` varchar(255) DEFAULT NULL,
  `harvesterType` varchar(255) DEFAULT NULL,
  `harvesterUsername` varchar(255) DEFAULT NULL,
  `indexPeriod` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `referringUrl` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Site
-- ----------------------------

-- ----------------------------
-- Table structure for `SiteMessage`
-- ----------------------------
DROP TABLE IF EXISTS `SiteMessage`;
CREATE TABLE `SiteMessage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `value` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of SiteMessage
-- ----------------------------

-- ----------------------------
-- Table structure for `Site_Messages_SiteMessage`
-- ----------------------------
DROP TABLE IF EXISTS `Site_Messages_SiteMessage`;
CREATE TABLE `Site_Messages_SiteMessage` (
  `Site_id` bigint(20) NOT NULL,
  `messages_id` bigint(20) NOT NULL,
  UNIQUE KEY `messages_id` (`messages_id`),
  KEY `FKEB20B225F90EAA1E` (`messages_id`),
  KEY `FKEB20B2252A1B33EA` (`Site_id`),
  KEY `FKEB20B22532A58AEB` (`messages_id`),
  KEY `FKEB20B22539E3EFBD` (`Site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Site_Messages_SiteMessage
-- ----------------------------

-- ----------------------------
-- Table structure for `Site_Tools`
-- ----------------------------
DROP TABLE IF EXISTS `Site_Tools`;
CREATE TABLE `Site_Tools` (
  `Site_id` bigint(20) NOT NULL,
  `tools` varchar(255) DEFAULT NULL,
  KEY `FK9BAF26E32A1B33EA` (`Site_id`),
  KEY `FK9BAF26E339E3EFBD` (`Site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Site_Tools
-- ----------------------------
INSERT INTO Site_Tools VALUES ('1', 'flow');
INSERT INTO Site_Tools VALUES ('1', 'structure');
INSERT INTO Site_Tools VALUES ('1', 'home');
INSERT INTO Site_Tools VALUES ('1', 'flowmap');
INSERT INTO Site_Tools VALUES ('1', 'topics');
INSERT INTO Site_Tools VALUES ('1', 'topicsmap');
INSERT INTO Site_Tools VALUES ('1', 'conceptmap');
INSERT INTO Site_Tools VALUES ('1', 'question');
INSERT INTO Site_Tools VALUES ('1', 'language');
INSERT INTO Site_Tools VALUES ('1', 'participation');

-- ----------------------------
-- Table structure for `TriggerQuestion`
-- ----------------------------
DROP TABLE IF EXISTS `TriggerQuestion`;
CREATE TABLE `TriggerQuestion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Question` longtext,
  `docId` varchar(255) DEFAULT NULL,
  `pattern` varchar(255) DEFAULT NULL,
  `producer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of TriggerQuestion
-- ----------------------------
