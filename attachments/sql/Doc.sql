-- phpMyAdmin SQL Dump
-- Host: localhost

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `glosser_test001`
--

-- --------------------------------------------------------

--
-- Table structure for table `Doc`
--

CREATE TABLE IF NOT EXISTS `Doc` (
  `id` varchar(255) NOT NULL,
  `revision` int(11) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `indexed` bit(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`,`revision`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
