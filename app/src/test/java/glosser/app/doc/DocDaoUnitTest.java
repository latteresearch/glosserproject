/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.doc;

import org.hibernate.cfg.Configuration;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DocDaoUnitTest {

	private DocDao docDao;
	private Doc doc1;
	private Doc doc2;
	private Doc doc3;

	@Before
	public void setUp() {
		docDao = new DocDao();
		docDao.setSessionFactory(new Configuration().configure("site-hibernate.cfg.xml").buildSessionFactory());

		doc1 = new Doc();
		doc1.setId("document:1");
		doc1.setRevision(0);
		doc1.setIndexed(true);
		docDao.saveDoc(doc1);

		doc2 = new Doc();
		doc2.setId("document:1");
		doc2.setRevision(1);
		doc2.setIndexed(true);
		docDao.saveDoc(doc2);

		doc3 = new Doc();
		doc3.setId("document:1");
		doc3.setRevision(2);
		doc3.setIndexed(false);
		docDao.saveDoc(doc3);
	}

	@Test
	public void shouldLoadAndUpdateDoc() {
		Doc loadedDoc = docDao.getDocWhereIdAndRevision(doc1.getId(), doc1.getRevision());
		assertThat(loadedDoc, equalTo(doc1));

		doc1.setContent("test");
		docDao.updateDoc(doc1);
		Doc updatedDoc = docDao.getDocWhereIdAndRevision(doc1.getId(), doc1.getRevision());
		assertThat(updatedDoc.getContent(), equalTo(doc1.getContent()));
	}

	@Test
	public void shouldReturnDocsWhereIndexed() {
		List<Doc> docs = docDao.getDocListWhereIndexed(true, 100);
		assertThat(docs.size(), is(2));
		assertThat(docs.contains(doc1), is(true));
		assertThat(docs.contains(doc2), is(true));

		docs = docDao.getDocListWhereIndexed(false, 100);
		assertThat(docs.size(), is(1));
		assertThat(docs.contains(doc3), is(true));
	}

    @Test
	public void shouldReturnIndexedDocsWhereMaxRevision() {
		List<Doc> docs = docDao.getIndexedDocListWhereIdAndMaxRevision(doc1.getId(), doc1.getRevision());
		assertThat(docs.size(), is(1));
		assertThat(docs.contains(doc1), equalTo(true));
	}

	@Test
	public void shouldReturnLatestIndexedRevisionOfDoc() {
		Doc latestIndexedDoc = docDao.getLatestIndexedDocWhereId(doc1.getId());
		assertThat(latestIndexedDoc, equalTo(doc2));
	}

	@Test
	public void shouldReturnLatestRevisionOfDoc() {
		Doc latestDoc = docDao.getLatestDocWhereId(doc1.getId());
		assertThat(latestDoc, equalTo(doc3));
	}
}
