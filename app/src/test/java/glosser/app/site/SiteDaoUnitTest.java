/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.site;

import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class SiteDaoUnitTest {

	private SiteDao siteDao;

	@After 
	public void cleanUp() {
		siteDao.deleteSites(siteDao.getAllSites());
	}

	@Before
	public void setUp() {
		siteDao = new SiteDao();
		siteDao.setSessionFactory(new AnnotationConfiguration().configure("glosser-hibernate.cfg.xml").buildSessionFactory());
	}

	@Test
	public void testDBconstraints() {
		Site site = new Site();
		site.setName("test");
		siteDao.save(site);
		long id = site.getId();
		assertTrue(id != 0);

		// check null constraint
		try {
			Site site2 = new Site();
			site.setName(null);
			siteDao.save(site2);
			fail();
		} catch (Exception e) {
			// continue
		}

		// check unique constraint
		try {
			Site site2 = new Site();
			site.setName("test");
			siteDao.save(site2);
			fail();
		} catch (Exception e) {
			// continue
		}
	}

	@Test
	public void testSaveLoadAndUpdateAndSite() {
		// save site
		Site site = new Site();
		site.setName("test");
		siteDao.save(site);
		long id = site.getId();
		assertTrue(id != 0);

		// load site
		Site s1 = siteDao.getAllSites().get(0);
		Site s2 = siteDao.getSite(id);
		Site s3 = siteDao.getSiteByName("test");
		assertEquals(site, s1);
		assertEquals(s1, s2);
		assertEquals(s2, s3);

		// update site
		site.setTitle("Title");
		siteDao.saveOrUpdate(site);
		Site s4 = siteDao.getSite(site.getId());
		assertEquals("Title", s4.getTitle());

		// delete site
		siteDao.delete(site);
		Site deletedSite = siteDao.getSite(id);
		assertNull(deletedSite);
	}

	@Test
	public void testSaveLoadAndUpdateSiteMessages() {
		Site site = new Site();
		site.setName("test2");

		SiteMessage sm = new SiteMessage();
		String key = "test.test";
		String value = "This is a long string. This is a long string. This is a long string. This is a long string. This is a long string. This is a long string. This is a long string. This is a long string. This is a long string. This is a long string.";
		sm.setCode(key);
		sm.setValue(value);
		LinkedList<SiteMessage> sms = new LinkedList<SiteMessage>();
		sms.add(sm);

		site.setMessages(sms);
		siteDao.save(site);
		long id = site.getId();

		// load site & messages
		Site s = siteDao.getSite(id);
		assertEquals(key, s.getMessages().get(0).getCode());
		assertEquals(value, s.getMessages().get(0).getValue());
	}
	
	@Test
	public void testSaveLoadAndUpdateSiteTools() {
		Site site = new Site();
		site.setName("test3");

		String tool1 = "tool1";
		String tool2 = "tool2";

		LinkedList<String> tools = new LinkedList<String>();
		tools.add(tool1);
		tools.add(tool2);

		site.setTools(tools);

		siteDao.save(site);
		long id = site.getId();

		// load site & tools
		Site s = siteDao.getSite(id);
		assertEquals(tool1, s.getTools().get(0));
		assertEquals(tool2, s.getTools().get(1));
	}

}
