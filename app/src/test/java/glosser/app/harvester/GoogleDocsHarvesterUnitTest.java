/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.harvester;

import glosser.app.doc.Doc;
import glosser.gdata.GoogleDocsServiceImpl;
import com.google.gdata.data.docs.DocumentEntry;
import com.google.gdata.util.ServiceException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class GoogleDocsHarvesterUnitTest {
	private static GoogleDocsServiceImpl googleDocsServiceImpl;
	private static DocumentEntry newDocumentEntry;
	private static String username;
	private static String password;
	
	@BeforeClass
	public static void setUp() throws IOException, ServiceException, ConfigurationException {
		PropertiesConfiguration config = new PropertiesConfiguration("glosser.properties");
		for(Iterator<String> keys = config.getKeys(); keys.hasNext();) {
			String key = keys.next();
			System.setProperty(key, config.getString(key));
		}

		username = System.getProperty("glosser.google.username");
		password = System.getProperty("glosser.google.password");
		googleDocsServiceImpl = new GoogleDocsServiceImpl(username, password);
		newDocumentEntry = googleDocsServiceImpl.createDocument("test document");
	}

	@Test
	public void shouldHarvesterDocument() throws Exception {
		GoogleDocsHarvester harvester = new GoogleDocsHarvester(username, password);
		
		boolean hasDocPermission = harvester.hasUserDocPermission(username, newDocumentEntry.getResourceId());
		assertThat(hasDocPermission, is(true));		
		
		List<Doc> documents = harvester.getUserDocs(username);
		assertThat(documents.size(), not(0));
		
		List<Doc> revisions = harvester.harvestDocRevisions(newDocumentEntry.getResourceId(), -1);
		assertThat(revisions.size(), is(1));
	}
	
	@AfterClass
	public static void cleanUp() throws IOException, ServiceException {
		googleDocsServiceImpl.delete(newDocumentEntry);
	}
}
