/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app;

import glosser.app.site.Site;
import glosser.app.site.SiteDao;
import com.google.gdata.util.AuthenticationException;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class GlosserUnitTest {

	private Site site;
	private SiteDao siteDao;

	@Before
	public void setUp() {
		siteDao = new SiteDao();
		siteDao.setSessionFactory(new AnnotationConfiguration().configure("glosser-hibernate.cfg.xml").buildSessionFactory());

		site = new Site();
		site.setName("testSite");
		site = siteDao.save(site);
	}

	@Test
	public void testAddAndRemoveSite() throws IOException, AuthenticationException {
		// get sites
		List<Site> addedSites = Glosser.getSites();
		assertThat(addedSites.size(), is(1));
		assertThat(addedSites.get(0), is(site));
		
		// get site
		Site addedSite = Glosser.getSite(site.getId());
		assertThat(addedSite, is(site));
		assertThat(addedSite.getDocDao(), not(nullValue()));
		assertThat(addedSite.getRepository(), not(nullValue()));

		// remove site
		siteDao.delete(addedSite);
		assertThat(Glosser.getSite(site.getId()), is(nullValue()));
	}
}
