/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app;

import glosser.app.doc.Doc;
import glosser.app.site.Site;
import org.apache.lucene.store.LockObtainFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tml.storage.importers.HtmlImporter;
import tml.storage.importers.TextImporter;

import java.util.List;

/**
 * In this thread Document is indexed by TML
 * @author rafa
 *
 */
public class IndexerThread extends Thread {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private boolean finish = false;
    private Site site;

	public IndexerThread(Site site) {
		this.site = site;
	}

	void finish() {
		finish = true;
	}

	private void indexDoc(Doc doc) {
		try {
			logger.debug("Indexing: site=" + site.getId() + ", doc=" + doc.tmlId());
			// clean content content
			String content = null;
			HtmlImporter htmlImporter = new HtmlImporter();
			try {
				content = htmlImporter.getCleanContent(doc.getContent());
			} catch (IllegalArgumentException ex) {
				content = new String();
			}
			// index doc
			site.getRepository().addDocument(doc.tmlId(), content, doc.getTitle(), "N/A", new TextImporter());
			doc.setIndexed(true);
			site.getDocDao().updateDoc(doc);
		} catch (LockObtainFailedException ex) {
			logger.error("Failed to index document: site=" + site.getId() + ", doc=" + doc.tmlId(), ex);
			return;
		} catch (Exception ex) {
			logger.error("Failed to index document: site=" + site.getId() + ", doc=" + doc.tmlId(), ex);
			doc.setIndexed(null);
			site.getDocDao().updateDoc(doc);
		}
	}

	@Override
	public synchronized void run() {

		while (!finish) {
			logger.debug("Indexing site docs: " + site.getId());
			List<Doc> docs = site.getDocDao().getDocListWhereIndexed(false, 100);
			for (Doc doc : docs) {
				this.indexDoc(doc);
			}
			try {
				sleep(site.getIndexPeriod());
			} catch (InterruptedException e) {
				// continue
			}
		}
	}
}
