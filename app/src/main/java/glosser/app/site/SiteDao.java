/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.site;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class SiteDao {

	private SessionFactory sessionFactory;

	public void delete(Site site) {
		Session session = this.getCurrentSession();
		session.beginTransaction();
		session.delete(site);
		session.getTransaction().commit();
	}

	public void deleteSites(List<Site> sites) {
		Session session = this.getCurrentSession();
		session.beginTransaction();
		for (Site site : sites) {
			session.delete(site);
		}
		session.getTransaction().commit();
	}

	public List<Site> getAllSites() {
		Session session = this.getCurrentSession();
		session.beginTransaction();
		List<Site> result = session.createCriteria(Site.class).list();
		session.getTransaction().commit();
		return result;
	}

	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public Site getSite(long id) {
		Session session = this.getCurrentSession();
		session.beginTransaction();
		Site site = (Site) session.createCriteria(Site.class)
                .add(Restrictions.idEq(id))
                .uniqueResult();
		session.getTransaction().commit();
		return site;
	}

	public Site getSiteByName(String name) {
		Session session = this.getCurrentSession();
		session.beginTransaction();
		Site site = (Site) session.createCriteria(Site.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();
		session.getTransaction().commit();
		return site;
	}

	public Site save(Site site) {
		Session session = this.getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(site);
			tx.commit();
		} catch (Exception ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new RuntimeException(ex);
		}
		return site;
	}

	public void saveOrUpdate(Site site) {
		Session session = this.getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(site);
		session.getTransaction().commit();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
