/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.site;

import glosser.app.HarvesterThread;
import glosser.app.doc.DocDao;
import glosser.app.harvester.Harvester;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import tml.storage.Repository;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
/**
 * Each subject can configure the Tools and harvesting parameters so are treated separately.
 * 
 * @author rafa
 *
 */
@Entity
public class Site implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, unique = true)
	private String name;

	/**
	 * harvest automatically or on-demand.
	 */
	private boolean autoHarvest = false;
	/**
	 * How often do we harvest?
	 */
	private long indexPeriod = 15000;
	/**
	 * Url where the documents are. (i.e http://docs.google.com)
	 */
	private String referringUrl;
	/**
	 * For example, title =  elec3610 where glosser_elec3610 is the database
	 */
	private String title;
	/**
	 * Google, Trac and inMemory?
	 */
	private String harvesterType;
	/**
	 * User (e.g. Google apps user that creates the documents)
	 */
	private String harvesterUsername;
	/**
	 * password
	 */
	private String harvesterPassword;

	@ElementCollection
	//@IndexColumn(name = "index")
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name = "Site_Tools")
	private List<String> tools = new LinkedList<String>();

	@OneToMany(cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name = "Site_Messages_SiteMessage")
	private List<SiteMessage> messages = new LinkedList<SiteMessage>();

	@Transient
	private boolean enforceDocPermissions = false;
	@Transient
	private DocDao docDao;
	@Transient
	private Harvester harvester;
	@Transient
	private HarvesterThread harvesterThread;
	@Transient
	private Repository repository;
	@Transient
	public static final String GOOGLE_HARVESTER_TYPE = "google";
	@Transient
	public static final String TRAC_HARVESTER_TYPE = "trac";

	public boolean enforceDocPermissions() {
		return enforceDocPermissions;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Site))
			return false;
		Site other = (Site) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public boolean getAutoHarvest() {
		return autoHarvest;
	}

	public DocDao getDocDao() {
		return docDao;
	}

	public Harvester getHarvester() {
		return harvester;
	}

	public String getHarvesterPassword() {
		return harvesterPassword;
	}

	public HarvesterThread getHarvesterThread() {
		return harvesterThread;
	}

	public String getHarvesterType() {
		return harvesterType;
	}

	public String getHarvesterUsername() {
		return harvesterUsername;
	}

	public long getId() {
		return id;
	}

	public long getIndexPeriod() {
		return indexPeriod;
	}

	public List<SiteMessage> getMessages() {
		return messages;
	}

	public String getName() {
		return name;
	}

	public String getReferringUrl() {
		return referringUrl;
	}

	public Repository getRepository() {
		return repository;
	}

	public String getTitle() {
		return title;
	}

	public List<String> getTools() {
		return tools;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	public void setAutoHarvest(boolean autoHarvest) {
		this.autoHarvest = autoHarvest;
	}

	public void setDocDao(DocDao docDao) {
		this.docDao = docDao;
	}

	public void setEnforceDocPermissions(boolean enforceDocPermissions) {
		this.enforceDocPermissions = enforceDocPermissions;
	}

	public void setHarvester(Harvester harvester) {
		this.harvester = harvester;
	}

	public void setHarvesterPassword(String harvesterPassword) {
		this.harvesterPassword = harvesterPassword;
	}

	public void setHarvesterThread(HarvesterThread harvesterThread) {
		this.harvesterThread = harvesterThread;
	}

	public void setHarvesterType(String harvesterType) {
		this.harvesterType = harvesterType;
	}

	public void setHarvesterUsername(String harvesterUsername) {
		this.harvesterUsername = harvesterUsername;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setIndexPeriod(long indexPeriod) {
		this.indexPeriod = indexPeriod;
	}

	public void setMessages(List<SiteMessage> messages) {
		this.messages = messages;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setReferringUrl(String referringUrl) {
		this.referringUrl = referringUrl;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTools(List<String> tools) {
		this.tools = tools;
	}
}
