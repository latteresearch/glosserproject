/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app;

import glosser.app.doc.DocDao;
import glosser.app.harvester.GoogleDocsHarvester;
import glosser.app.harvester.Harvester;
import glosser.app.site.Site;
import glosser.app.site.SiteDao;
import com.google.gdata.util.AuthenticationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tml.storage.Repository;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/**
 * Main Glosser class. 
 * 
 * Each site (i.e. subject) has its own database. 
 * Multiple database is a legacy from when we used Trac and each Trac site had it's own database that we needed to access documents. 
 * Now that we're using Google Docs and storing a copy of the documents locally, it's no longer necessary.
 * 
 * Glosser is instantiated once and then a factory for each Site.
 * There is a database called Glosser. The main table is Site. Each Site has an entry in that table. 
 * Each Site has its own database to store the documents for that Site.
 * <p>
 * Each Site has its own Tools {e.g participation, language}. Each Site dabatase has all the documents downloaded by the harvester.
 * @author rafa
 *
 */
public final class Glosser {
    private static final Logger logger = LoggerFactory.getLogger(Glosser.class);
    private PropertiesConfiguration config;
    private Map<Long, IndexerThread> siteIndexerThreads = new HashMap<Long, IndexerThread>();
    private Map<Long, HarvesterThread> siteHarvesterThreads = new HashMap<Long, HarvesterThread>();
    private Map<Long, Repository> siteRepositories = new HashMap<Long, Repository>();
    private Map<Long, DocDao> siteDocDaos = new HashMap<Long, DocDao>();
    private Map<String, Harvester> harvesters = new HashMap<String, Harvester>();
    private SiteDao siteDao;

    private static class LazyHolder {
        public static final Glosser GLOSSER = new Glosser();
    }

    private static Glosser Glosser() {
        return LazyHolder.GLOSSER;
    }

    private Glosser() {
        loadProperties();
        loadSiteDao();
    }

    private void loadProperties() {
        try {
            config = new PropertiesConfiguration("glosser.properties");
            for(Iterator<String> keys = config.getKeys(); keys.hasNext();) {
                String key = keys.next();
                String value = config.getString(key);
                System.setProperty(key, value);
                logger.debug("Set property: " + key + "=" + value);
            }
        } catch (Throwable e) {
            logger.error("Failed to load glosser.properties", e);
        }
    }

    /**
     * each Site has its own DAO
     */
	private void loadSiteDao() {
		try {
			siteDao = new SiteDao();
			siteDao.setSessionFactory(new AnnotationConfiguration().configure("glosser-hibernate.cfg.xml").buildSessionFactory());
		} catch (Throwable e) {
			logger.error("Failed to initialise glosser-hibernate.cfg.xml hibernate session factory", e);
		}
	}

    public static Site getSite(long siteId) {
        Site site = Glosser().siteDao.getSite(siteId);
        if (site == null) {
            Glosser().removeSite(siteId);
            return null;
        }

        try {
            return Glosser().fillSite(site);
        } catch (Exception e) {
            logger.error("Failed to load Site: " + siteId, e);
            throw new RuntimeException(e);
        }
    }

    public static List<Site> getSites() {
        List<Site> sites = Glosser().siteDao.getAllSites();
        return sites;
    }

    /**
     * Each Site has its own DB connection, harvester thread, indexer thread
     * Repository is a Lucene repository
     * @param site
     * @return
     * @throws IOException
     * @throws AuthenticationException
     */
	private synchronized Site fillSite(Site site) throws IOException, AuthenticationException {
		// create DocDao from site id
		if (!siteDocDaos.containsKey(site.getId())) {
			org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration().configure("site-hibernate.cfg.xml");
			configuration.setProperty("hibernate.connection.url", (configuration.getProperty("hibernate.connection.url") + "_" + site.getName()));
			SessionFactory sessionFactory = configuration.buildSessionFactory();
            // TODO We need to check if the connection has been established!
            DocDao docDao = new DocDao();
		    docDao.setSessionFactory(sessionFactory);
			site.setDocDao(docDao);
			siteDocDaos.put(site.getId(), docDao);
		} else {
			site.setDocDao(siteDocDaos.get(site.getId()));
		}

		// create Repository from site id
		if (!siteRepositories.containsKey(site.getId())) {
			File folder = new File(config.getString("glosser.repository.home") + "/" + site.getName());
			folder.mkdirs();
			Repository repository = new Repository(folder.getPath());
			site.setRepository(repository);
			siteRepositories.put(site.getId(), repository);
		} else {
			site.setRepository(siteRepositories.get(site.getId()));
		}

		// create harvester
		String harvesterKey = site.getHarvesterType() + site.getHarvesterUsername() + site.getHarvesterPassword();
		if (!harvesters.containsKey(harvesterKey)) {
			if (Site.GOOGLE_HARVESTER_TYPE.equalsIgnoreCase(site.getHarvesterType())) {
				GoogleDocsHarvester harvester = new GoogleDocsHarvester(site.getHarvesterUsername(), site.getHarvesterPassword());
				site.setHarvester(harvester);
				harvesters.put(harvesterKey, harvester);
			}
		} else {
			site.setHarvester(harvesters.get(harvesterKey));
		}
		
		// create site harvester
		if (!siteHarvesterThreads.containsKey(site.getId())) {
			HarvesterThread harvesterThread = new HarvesterThread(site);
			siteHarvesterThreads.put(site.getId(), harvesterThread);
			site.setHarvesterThread(harvesterThread);
			harvesterThread.start();
		} else {
			site.setHarvesterThread(siteHarvesterThreads.get(site.getId()));
		}

		// create site indexer
		if (!siteIndexerThreads.containsKey(site.getId())) {
			IndexerThread siteIndexer = new IndexerThread(site);
			siteIndexerThreads.put(site.getId(), siteIndexer);
			siteIndexer.start();
		}

		return site;
	}

	private synchronized void removeSite(long siteId) {
		// finish harvester thread
		HarvesterThread harvesterThread = siteHarvesterThreads.remove(siteId);
		if (harvesterThread != null) {
			harvesterThread.finish();
		}
		// finish indexer thread
		IndexerThread indexerThread = siteIndexerThreads.remove(siteId);
		if (indexerThread != null) {
			indexerThread.finish();
		}
		siteDocDaos.remove(siteId);
		siteRepositories.remove(siteId);
	}
	
	@Override 
	protected void finalize() {
		// finish harvester threads
		for(HarvesterThread harvesterThread : siteHarvesterThreads.values()) {
			harvesterThread.finish();
		}
		// finish indexer threads
		for(IndexerThread indexerThread : siteIndexerThreads.values()) {
			indexerThread.finish();
		}
	}
}
