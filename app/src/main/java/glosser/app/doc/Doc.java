/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.doc;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Doc implements Serializable {

	private static final long serialVersionUID = 1L;
	protected String id;
	protected String content;
	protected Date date = new Date();
	protected String title;
	protected String plainText;
	protected int revision;
	protected List<Doc> revisions;
	protected Boolean indexed = false;
	protected String author;
	protected String owner;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Doc) {
			Doc other = (Doc) obj;
			return new EqualsBuilder().append(this.getId(), other.getId()).append(this.getRevision(), other.getRevision()).isEquals();
		}
		return false;
	}

	public String getAuthor() {
		return author;
	}

	public String getContent() {
		return content;
	}

	public Date getDate() {
		return date;
	}

	public String getId() {
		return id;
	}

	public Boolean getIndexed() {
		return indexed;
	}

	public String getOwner() {
		return owner;
	}

	public String getPlainText() {
		return plainText;
	}

	public int getRevision() {
		return revision;
	}

	public List<Doc> getRevisions() {
		return revisions;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getId()).append(this.getRevision()).toHashCode();
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setIndexed(Boolean indexed) {
		this.indexed = indexed;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}

	public void setRevision(int revision) {
		this.revision = revision;
	}

	public void setRevisions(List<Doc> revisions) {
		this.revisions = revisions;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String tmlId() {
		String tmlId = this.getId() + "-" + this.getRevision();
		tmlId = tmlId.replaceAll("[ :._]", "");
		return tmlId;
	}
}
