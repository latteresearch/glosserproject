/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.doc;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;

public class DocDao implements Serializable {
	private static final long serialVersionUID = -3427710839392677933L;
	private SessionFactory sessionFactory;				            //this is Serializable
	private static final Logger logger = LoggerFactory.getLogger(DocDao.class); //Logger from SLF4J <1.5.3 IS NOT SERIALIZABLE
										    // THEREFORE MAKE THEM STATIC, ONE PER CLASS
										    // STATIC FIELDS ARE NOT SERIALIZED WITH THE OBJECTS!
    
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public List<Doc> getDocListWhereIndexed(boolean indexed, int max) {
		logger.debug("Getting the doc list where indexed: " + indexed);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		List<Doc> result = session.createCriteria(Doc.class)
                .add(Property.forName("indexed").eq(indexed))
                .setMaxResults(max)
                .list();
		session.getTransaction().commit();
		return result;
	}

	public Doc getDocWhereIdAndRevision(String id, int revision) {
		logger.debug("Getting doc where id: " + id + ", revision: " + revision);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		Doc doc = (Doc) session.createCriteria(Doc.class)
                .add(Property.forName("id").eq(id))
                .add(Property.forName("revision").eq(revision))
                .uniqueResult();
		session.getTransaction().commit();
		return doc;
	}

	public List<Doc> getIndexedDocListWhereId(String id) {
		logger.debug("Getting indexed doc list where id: " + id);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		List<Doc> result = session.createCriteria(Doc.class)
                .add(Property.forName("id").eq(id))
                .add(Property.forName("indexed").eq(true))
                .list();
		session.getTransaction().commit();
		return result;
	}

	public List<Doc> getIndexedDocListWhereIdAndMaxRevision(String id, int revision) {
		logger.debug("Getting indexed doc list where id: " + id + ", max revision: " + revision);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		List<Doc> result = session.createCriteria(Doc.class)
                .add(Property.forName("id").eq(id))
                .add(Property.forName("revision").le(revision))
                .add(Property.forName("indexed").eq(true))
                .addOrder(Order.asc("revision"))
                .list();
		session.getTransaction().commit();
		return result;
	}

	public List<Doc> getLatestDocsWhereOwner(String owner) {
		logger.debug("Getting latest docs where owner: " + owner);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		List<Doc> result = session.createQuery("from Doc doc where revision=(select max(revision) from Doc where id=doc.id)")
                .setParameter("owner", owner)
                .list();
		session.getTransaction().commit();
		return result;
	}

	public Doc getLatestDocWhereId(String id) {
		logger.debug("Getting the latest doc where id: " + id);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		Doc doc = (Doc) session.createQuery("from Doc where id=:id AND revision=(select max(revision) from Doc where id=:id)")
                .setParameter("id", id)
                .uniqueResult();
		session.getTransaction().commit();
		return doc;
	}

	public Doc getLatestIndexedDocWhereId(String id) {
		logger.debug("Getting the latest indexed doc where id: " + id);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		Doc doc = (Doc) session.createQuery("from Doc where id=:id AND indexed=:indexed AND revision=(select max(revision) from Doc where id=:id AND indexed=:indexed)")
                .setParameter("id", id).setParameter("indexed", true)
                .uniqueResult();
		session.getTransaction().commit();
		return doc;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void saveDoc(Doc doc) {
		logger.debug("Saving doc: " + doc.getId());
		Session session = this.getCurrentSession();
		session.beginTransaction();
		session.save(doc);
		session.getTransaction().commit();
	}

	public void saveDocs(List<Doc> docs) throws Exception {
		logger.debug("Saving doc list: " + docs.size());
		Session session = this.getCurrentSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			for (Doc doc : docs) {
				session.save(doc);
			}
			tx.commit();
		} catch (Exception ex) {
			logger.error("Failed to save docs", ex);
			if (tx != null) {
				tx.rollback();
			}
			throw ex;
		}
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void updateDoc(Doc doc) {
		logger.debug("Updating doc: " + doc.getId());
		Session session = this.getCurrentSession();
		session.beginTransaction();
		session.update(doc);
		session.getTransaction().commit();
	}
}
