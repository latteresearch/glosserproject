/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.doc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Using Regex, counts words, sentences and paragraphs in a document
 * @author rafa
 *
 */
public class DocStatistics {
	private static final String WORD = "[^\\s]+";
	private static final String SENTENCE = "[\\.!?]([\\s]+|$)|[\\n\\r\\t]+";
	private static final String PARAGRAPH = ".+";

	private Pattern wordPattern = Pattern.compile(WORD);
	private Pattern sentencePattern = Pattern.compile(SENTENCE);
	private Pattern paragraphPattern = Pattern.compile(PARAGRAPH);

	private Doc doc;

	public DocStatistics(Doc doc) {
		this.doc = doc;
	}

	private int countMatches(Pattern pattern) {
		int count = 0;
		Matcher m = pattern.matcher(doc.getPlainText());
		while (m.find()) {
			count++;
		}
		return count;
	}

	public Doc getOriginalSegment(String plainText) {
		Doc originalDoc = new Doc();
		for (Doc doc : this.doc.getRevisions()) {
			if (doc.getPlainText().contains(plainText)) {
				originalDoc.setAuthor(doc.getAuthor());
				originalDoc.setTitle(doc.getTitle());
				originalDoc.setPlainText(plainText);
				originalDoc.setDate(doc.getDate());
				originalDoc.setRevision(doc.getRevision());
				break;
			}
		}
		return originalDoc;
	}

	public int getParagraphCount() {
		return countMatches(paragraphPattern);
	}

	public int getSentenceCount() {
		return countMatches(sentencePattern);
	}

	public int getWordCount() {
		return countMatches(wordPattern);
	}
}
