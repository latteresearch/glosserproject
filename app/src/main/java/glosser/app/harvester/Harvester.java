/*******************************************************************************
 * Copyright 2010, 2011. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.harvester;

import glosser.app.doc.Doc;

import java.util.List;

/**
 * Interface that needs to be implemented for every Harvester.
 * 
 * Documents that Glosser processes will be in a content repository at a different system or on the cloud. 
 * Glosser and the document repository interact through the Harvester interface. 
 * The most commonly use implementation as of 2011 is Google Docs.  
 * A Harvester class encapsulates the functionality for downloading documents from a particular data source. 
 * Each Site requires a Harvester to download the document content and metadata. 
 * This design allows for additional Harvesters to be implemented, which could potentially provide support for any 
 * type of document data source or format available on the web. 
 * 
 * Currently, two Harvesters have been implemented to support documents from Google Docs and Trac wiki.
 * 
 * @author rafa
 *
 */
public interface Harvester {
	/**
	 * gets all Docs for user
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public List<Doc> getUserDocs(String userId) throws Exception;
	/**
	 * gets all revisions?
	 * @param id
	 * @param minRevision
	 * @return
	 * @throws Exception
	 */
	public List<Doc> harvestDocRevisions(String id, int minRevision) throws Exception;
	
	/**
	 * 
	 * @param userId
	 * @param docId
	 * @return
	 * @throws Exception
	 */
	public boolean hasUserDocPermission(String userId, String docId) throws Exception;
}
