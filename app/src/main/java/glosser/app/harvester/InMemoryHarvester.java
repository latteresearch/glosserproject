/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.harvester;

import glosser.app.doc.Doc;

import java.util.ArrayList;
import java.util.List;

/**
 *  The InMemoryHarvester is basically a stub. It could be used for testing or setting up a demo site. 
 *  We used to use it to load up documents for peer reviews.
 * @author rafa
 *
 */
public class InMemoryHarvester implements Harvester {

	private List<Doc> docs;

	@Override
	public List<Doc> getUserDocs(String userId) {
		return docs;
	}

	@Override
	public List<Doc> harvestDocRevisions(String id, int minRevision) {
		for (Doc doc : docs) {
			if (doc.getId().equals(id) && doc.getRevisions() != null) {
                return doc.getRevisions();
			}
		}
		return new ArrayList<Doc>();
	}

	@Override
	public boolean hasUserDocPermission(String userId, String docId) {
		return true;
	}

	public void setDocs(List<Doc> docs) {
		this.docs = docs;
	}
}
