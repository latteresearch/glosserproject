/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.harvester;

import glosser.app.doc.Doc;
import glosser.app.doc.DocDao;

import java.util.ArrayList;
import java.util.List;

public class FileHarvester implements Harvester {

	private DocDao docDao;

	public FileHarvester(DocDao docDao) {
		this.docDao = docDao;
	}

	@Override
	public List<Doc> getUserDocs(String userId) {
		List<Doc> docs = docDao.getLatestDocsWhereOwner(userId);
		return docs;
	}

	@Override
	public List<Doc> harvestDocRevisions(String id, int minRevision) {
		// TODO
		return new ArrayList<Doc>();
	}

	@Override
	public boolean hasUserDocPermission(String userId, String docId) {
		Doc doc = docDao.getLatestDocWhereId(docId);
		if (doc.getOwner() == null || doc.getOwner().equals(userId)) {
			return true;
		}
		return false;
	}
}
