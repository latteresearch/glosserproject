/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.harvester.trac;

import glosser.app.doc.Doc;
import info.bliki.wiki.model.WikiModel;

import java.util.Date;

public class TracWiki extends Doc {

	private static final long serialVersionUID = 1L;
	private Integer readonly;
	private long time;

	public Integer getReadonly() {
		return readonly;
	}

	public long getTime() {
		return time;
	}

	public String htmlContent() {
		// workarounds for bliki
		String content = getContent().replaceAll("\\[\\[.*\\]\\]", ""); // remove macros
		content = content.replaceAll("(?m)(^ +)", ""); // remove leading whitespaces
		content = content.replaceAll("\\[wiki:[^\\s]+ (.+)\\]", "$1"); // remove internal wiki links
		content = content.replaceAll("\\{{3}\\s+#!html\\s+(.+)\\s+\\}{3}", "$1"); // remove wiki html tags
		// convert wiki formatted text to html formatted text
		WikiModel wikiModel = new WikiModel(null, null);
		String htmlContent = wikiModel.render(content);
		htmlContent = htmlContent.replaceAll("</?pre>", ""); // remove <pre> tag

		return htmlContent;
	}

	public void setReadonly(Integer readonly) {
		this.readonly = readonly;
	}

	public void setTime(long time) {
		this.date = new Date(time * 1000);
		this.time = time;
	}
}
