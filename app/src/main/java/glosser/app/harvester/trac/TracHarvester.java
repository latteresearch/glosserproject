/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.harvester.trac;

import glosser.app.doc.Doc;
import glosser.app.harvester.Harvester;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TracHarvester implements Harvester {
    private final Logger logger = LoggerFactory.getLogger(getClass());
	private SessionFactory sessionFactory;

	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	private List<TracWiki> getLatestWikiList() {
		logger.debug("Getting the latest list of wikis");
		DetachedCriteria subquery = DetachedCriteria.forClass(TracWiki.class).add(Property.forName("id").eqProperty("wiki.id")).setProjection(Projections.max("revision"));
		Session session = this.getCurrentSession();
		session.beginTransaction();
		List<TracWiki> wikis = session.createCriteria(TracWiki.class, "wiki").add(Property.forName("revision").eq(subquery)).addOrder(Order.asc("time")).list();
		session.getTransaction().commit();
		return wikis;
	}

	private TracWiki getLatestWikiWhereId(String id) {
		logger.debug("Getting the latest wiki where id: " + id);
		DetachedCriteria subquery = DetachedCriteria.forClass(TracWiki.class).add(Property.forName("id").eqProperty("wiki.id")).setProjection(Projections.max("revision"));
		Session session = this.getCurrentSession();
		session.beginTransaction();
		TracWiki wiki = (TracWiki) session.createCriteria(TracWiki.class, "wiki").add(Property.forName("id").eq(id)).add(Property.forName("revision").eq(subquery)).uniqueResult();
		session.getTransaction().commit();
		return wiki;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public List<Doc> getUserDocs(String userId) {
		List<Doc> docs = new ArrayList<Doc>();
		List<TracWiki> wikis = this.getLatestWikiList();
		for (TracWiki wiki : wikis) {
			Doc doc = new Doc();
			doc.setId(wiki.getId());
			doc.setRevision(wiki.getRevision());
			doc.setDate(wiki.getDate());
			docs.add(doc);
		}
		return docs;
	}

	@Override
	public List<Doc> harvestDocRevisions(String id, int minRevision) {
		logger.debug("Getting wiki list where id: " + id + ", min version: " + minRevision);
		Session session = this.getCurrentSession();
		session.beginTransaction();
		List<TracWiki> wikis = session.createCriteria(TracWiki.class).add(Property.forName("id").eq(id)).add(Property.forName("revision").ge(minRevision)).addOrder(Order.asc("revision")).list();
		session.getTransaction().commit();
		List<Doc> docs = new ArrayList<Doc>();
		for (TracWiki wiki : wikis) {
			Doc doc = new Doc();
			doc.setId(wiki.getId());
			doc.setRevision(wiki.getRevision());
			doc.setAuthor(wiki.getAuthor());
			doc.setTitle(wiki.getId());
			doc.setDate(wiki.getDate());
			doc.setContent(wiki.htmlContent());
			docs.add(doc);
		}	
		return docs;
	}

	@Override
	public boolean hasUserDocPermission(String userId, String docId) {
		if (this.getLatestWikiWhereId(docId) != null) {
			return true;
		} else {
			return false;
		}
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
