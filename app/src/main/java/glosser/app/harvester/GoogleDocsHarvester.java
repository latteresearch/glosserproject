/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app.harvester;

import glosser.app.doc.Doc;
import glosser.gdata.GoogleDocsServiceImpl;
import com.google.gdata.data.acl.AclRole;
import com.google.gdata.data.docs.DocumentEntry;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.data.docs.RevisionEntry;
import com.google.gdata.util.AuthenticationException;
import org.apache.commons.lang.StringUtils;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.lexer.Lexer;
import org.htmlparser.tags.BodyTag;
import org.htmlparser.util.NodeList;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Harvester for Google Docs. 
 * 
 * Gets documents and revisions from Google docs. 
 * @author rafa
 *
 */
public class GoogleDocsHarvester implements Harvester {

	private GoogleDocsServiceImpl googleDocsServiceImpl;
	
	public GoogleDocsHarvester(String username, String password) throws AuthenticationException, MalformedURLException {
		googleDocsServiceImpl = new GoogleDocsServiceImpl(username, password);
	}

	private String downloadDocumentHtml(RevisionEntry entry) throws Exception {
		String html = googleDocsServiceImpl.downloadDocumentHtml(entry);
		Parser parser = new Parser(new Lexer(html));
		NodeFilter bodyFilter = new TagNameFilter("BODY");
		NodeList bodyNodes = parser.extractAllNodesThatMatch(bodyFilter);
		BodyTag body = (BodyTag) bodyNodes.elementAt(0);
		String content = body.getChildrenHTML();
		return content;
	}

	@Override
	public List<Doc> getUserDocs(String userId) throws Exception {
		List<DocumentListEntry> entries = googleDocsServiceImpl.getDocumentsWhereUserPermission(userId, AclRole.READER);
		ArrayList<Doc> docs = new ArrayList<Doc>();
		for (DocumentListEntry entry : entries) {
			Doc doc = new Doc();
			doc.setId(entry.getResourceId());
			doc.setTitle(entry.getTitle().getPlainText());
			docs.add(doc);
		}
		return docs;
	}

	/**
	 * returns the List of revisions
	 */
			
	@Override
	public List<Doc> harvestDocRevisions(String id, int minRevision) throws Exception {
		ArrayList<Doc> docs = new ArrayList<Doc>();
		DocumentEntry documentEntry = googleDocsServiceImpl.getDocument(id);
		Iterator<RevisionEntry> revisionEntries = googleDocsServiceImpl.getDocumentRevisions(documentEntry).iterator();
		int previousRevision = 0;
		RevisionEntry previousEntry = null;
		while (revisionEntries.hasNext()) {
			RevisionEntry revisionEntry = revisionEntries.next();
			int revision = Integer.valueOf(StringUtils.substringAfterLast(revisionEntry.getId(), "/revisions/"));
			if (revision >= minRevision) {
				// cluster consecutive revisions based on the author and date.
				if(previousEntry != null 
						&& (!revisionEntry.getModifyingUser().getName().equals(previousEntry.getModifyingUser().getName()) 
								|| revisionEntry.getEdited().getValue() - previousEntry.getEdited().getValue() > 30 * 60 * 1000 )) {
					// add previous revision
					Doc doc = new Doc();
					doc.setId(id);
					doc.setRevision(previousRevision);
					doc.setTitle(documentEntry.getTitle().getPlainText());
					doc.setAuthor(previousEntry.getModifyingUser().getName());
					doc.setDate(new Date(previousEntry.getEdited().getValue()));
					doc.setContent(this.downloadDocumentHtml(previousEntry));
					docs.add(doc);
				}
				if(!revisionEntries.hasNext()) {
					// add final revision
					Doc doc = new Doc();
					doc.setId(id);
					doc.setRevision(revision);
					doc.setTitle(documentEntry.getTitle().getPlainText());
					doc.setAuthor(revisionEntry.getModifyingUser().getName());
					doc.setDate(new Date(revisionEntry.getEdited().getValue()));
					doc.setContent(this.downloadDocumentHtml(revisionEntry));
					docs.add(doc);
				}
				previousRevision = revision;
				previousEntry = revisionEntry;
			}
		}
		return docs;
	}

	@Override
	public boolean hasUserDocPermission(String userId, String docId) throws Exception {
		return googleDocsServiceImpl.hasDocumentPermission(docId, userId, AclRole.OWNER);
	}
}
