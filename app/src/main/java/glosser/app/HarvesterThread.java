/*******************************************************************************
 * Copyright 2010, 2011. Stephen O'Rouke. The University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * - Contributors
 * 	Stephen O'Rourke
 * 	Jorge Villalon (CMM)
 * 	Ming Liu (AQG)
 * 	Rafael A. Calvo
 * 	Marco Garcia
 ******************************************************************************/
package glosser.app;

import glosser.app.doc.Doc;
import glosser.app.site.Site;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Each Site has its own HarvesterThread that queues the download of documents and revisions 
 * @author rafa
 *
 */
public class HarvesterThread extends Thread {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private BlockingQueue<String> docQueue = new LinkedBlockingQueue<String>();
    private boolean finish = false;
    private Site site;

	public HarvesterThread(Site site) {
		this.site = site;
	}

	void finish() {
		finish = true;
	}

	private void harvestDoc(String docId) {
		int minRevision = 0;
		try {
			// get latest doc revision
			Doc latestDoc = site.getDocDao().getLatestDocWhereId(docId);
			if (latestDoc != null) {
				minRevision = latestDoc.getRevision() + 1;
			}
			// harvest new doc revisions
			List<Doc> docRevisions = site.getHarvester().harvestDocRevisions(docId, minRevision);
			if (docRevisions.size() > 0) {
				site.getDocDao().saveDocs(docRevisions);
			}
		} catch (Exception e) {
			logger.error("Failed to harvest document: site=" + site.getId() + ", doc=" + docId + ", minRevision=" + minRevision, e);
		}
	}

	public boolean isDocQueued(String docId) {
		return docQueue.contains(docId);
	}
	
	public void queueDoc(String docId) {
		docQueue.add(docId);
	}

	@Override
	public synchronized void run() {
		while (!finish) {
			logger.debug("Harvesting site docs: " + site.getId());
			// harvest next doc
			try {
				this.harvestDoc(docQueue.take());
			} catch (InterruptedException e) {
				continue;
			}
		}
	}
}
